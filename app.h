/****************************************************************************
 * watch/app.h
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

#ifndef __WATCH_APP_H__
#define __WATCH_APP_H__

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <time.h>

#include <lvgl/lvgl.h>

#include "settings.h"
#include "input.h"
#include "display.h"
#include "watchdog.h"
#include "backlight.h"
#include "motor.h"

#include "devices/battery.h"
#if 0
#include "devices/baro.h"
#include "devices/buzzer.h"
#include "devices/mag.h"
#include "devices/acc.h"
#include "devices/odometer.h"
#include "devices/world_clock.h"
#endif

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Type Definitions
 ****************************************************************************/

namespace watch
{
  class Screen;

  typedef struct defered_task_s defered_task_t;

  class App
  {
    public:
      enum ScreenID
      {
        SCREEN_CLOCK,
#if 0
        SCREEN_SPEED, SCREEN_BATTERY, SCREEN_TEMP, SCREEN_MAG,
        SCREEN_SUNLIGHT, SCREEN_DATA,
        SCREEN_SENSORS,
#endif
        SCREEN_SETTINGS,
        MAX_SCREENS
      };

      App(void);
      ~App(void);

      bool initialize(void);
      void spin(void);
      void set_screen(ScreenID page);

      void set_next_screen(void);
      void set_prev_screen(void);

      ScreenID current_screen(void);

      void set_refresh_time(int seconds, const timespec &start = {},
                            bool absolute = false);



      Display display;
      Input input;

      Battery battery;
      Motor motor;
#if 0
      Baro baro;
      Buzzer buzzer;
      Mag mag;
      Acc acc;
      Odometer odometer;
      WorldClock world_clock;
#endif
      Watchdog watchdog;

      DeferedTask alarm_task, alarm_feedback_task;

      bool in_settings = false;
      bool should_stop = false;

      void task_lock(void);
      void task_unlock(void);

    private:
      /* UI handling */

      Screen* screens[MAX_SCREENS] = { nullptr };
      ScreenID screen = SCREEN_CLOCK;

      void update_screen(void);

      /* Watchdog */

      lv_task_t *watchdog_task = nullptr;
      void setup_watchdog_task(void);

      /* periodic timer handling */

      DeferedTask refresh_task;
      void setup_periodic_timer(void);

      void setup_sigint(void);
      void setup_defered_task(void);

      /* power handling */

      void sleep(void);
      void power_management(void);

      pthread_mutex_t task_mutex = PTHREAD_MUTEX_INITIALIZER;
  };
}

/****************************************************************************
 * Public Data
 ****************************************************************************/

namespace watch
{
  extern App* g_app;
}

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif // __WATCH_APP_H__
