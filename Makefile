include $(APPDIR)/Make.defs

# Application Settings

CONFIG_APP_WATCH_PRIORITY ?= SCHED_PRIORITY_DEFAULT
CONFIG_APP_WATCH_STACKSIZE ?= 2048

APPNAME = watch

PRIORITY  = $(CONFIG_APP_WATCH_PRIORITY)
STACKSIZE = $(CONFIG_APP_WATCH_STACKSIZE)

# Sources

ASRCS =
CSRCS =
CXXSRCS = app.cxx \
          display.cxx \
          input.cxx \
          defered_task.cxx \
          watchdog.cxx \
          backlight.cxx \
          motor.cxx
          # settings.cxx

MAINSRC = main.cxx

PROGNAME = watch
MODULE = CONFIG_APP_WATCH

include fonts/Make.defs
include screens/Make.defs
include devices/Make.defs

include $(APPDIR)/Application.mk

CFLAGS += -I$(APPDIR)/external/watch
CXXFLAGS += -I$(APPDIR)/external/watch

