/****************************************************************************
 * /home/v01d/coding/nuttx_dk08/extra_apps/watch/defered_task.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include "defered_task.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

watch::DeferedTask::DeferedTask(void)
{
  struct sigevent evp;
  evp.sigev_notify = SIGEV_SIGNAL;
  evp.sigev_signo = SIGUSR2;
  evp.sigev_value.sival_ptr = this;

  timer_create(CLOCK_REALTIME, &evp, &timer);
}

watch::DeferedTask::~DeferedTask()
{
  timer_delete(timer);
}

watch::DeferedTask::operator bool()
{
  return is_armed();
}

void watch::DeferedTask::program(lv_async_cb_t _cb, void *_user_data,
                                 const struct itimerspec& _timeout,
                                 bool _absolute)
{
  if (is_armed())
    {
      cancel();
    }

  printf("programming cb: %i\n", _cb);
  cb = _cb;
  user_data = _user_data;

  set_timeout(_timeout, _absolute);
}

bool watch::DeferedTask::is_armed(void)
{
  struct itimerspec t;
  timer_gettime(timer, &t);

  return (t.it_value.tv_sec != 0 && t.it_value.tv_nsec != 0);
}

void watch::DeferedTask::set_timeout(struct itimerspec _timeout,
                                     bool _absolute)
{
  if (!cb)
    {
      return;
    }

  timeout = _timeout;
  absolute = _absolute;

  timer_settime(timer, absolute ? TIMER_ABSTIME : 0, &timeout, NULL);
}

void watch::DeferedTask::reset(void)
{
  if (is_armed())
    {
      timer_settime(timer, absolute ? TIMER_ABSTIME : 0, &timeout, NULL);
    }
}

void watch::DeferedTask::cancel(void)
{
  struct itimerspec it;
  it.it_value.tv_sec = 0;
  it.it_value.tv_nsec = 0;
  timer_settime(timer, 0, &it, NULL);
}



