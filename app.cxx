/****************************************************************************
 * watch/app.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <sys/boardctl.h>
#include <signal.h>
#include <sys/time.h>
#include <arch/board/board.h>

#include "app.h"
#include "defered_task.h"

#include "screens/splash_screen.h"
#include "screens/clock_screen.h"
#include "screens/settings_screen.h"
#if 0
#include "screens/speed_screen.h"
#include "screens/battery_screen.h"
#include "screens/temp_screen.h"
#include "screens/mag_screen.h"
#include "screens/sunlight_screen.h"
#include "screens/data_screen.h"
#endif

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define ENABLE_MONITOR
#define DEBUG_PRINTS 0

/****************************************************************************
 * Private Type Declarations
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

watch::App* watch::g_app = NULL;

/****************************************************************************
 * Private Functions
 ****************************************************************************/

static void sigint_handler(int signo, FAR siginfo_t *info, FAR void *ucontext)
{
  if (watch::g_app)
  {
    watch::g_app->should_stop = true;
  }
}

static void defered_task_handler(int signo, FAR siginfo_t *info, FAR void *ucontext)
{
  watch::DeferedTask *t = (watch::DeferedTask *)info->si_value.sival_ptr;

  /* program the immediate (defered) execution */

  if (t->cb)
    {
      watch::g_app->task_lock();
      printf("adding async call to: %i\n", t->cb);
      lv_async_call(t->cb, t->user_data);
      watch::g_app->task_unlock();
    }
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

watch::App::App(void)
{
  g_app = this;
}

watch::App::~App()
{
  for (int i = 0; i < MAX_SCREENS; i++)
  {
    delete screens[i];
  }

  watch::g_app = nullptr;
}

bool watch::App::initialize(void)
{  
  /* initialize littlevgl */

  lv_init();

  /* initialize display */

  if (!display.initialize())
    {
      return false;
    }

  /* initialize input system */

  if (!input.initialize())
    {
      return false;
    }

#if 0
  /* initialize settings module */

  settings.initialize();

  /* initialize devices */

  baro.initialize();
#endif
  if (!watchdog.initialize())
    {
      return false;
    }

  if (!battery.initialize())
    {
      return false;
    }

  if (!motor.initialize())
    {
      return false;
    }
#if 0
  buzzer.initialize();
  mag.initialize();
  acc.initialize();
  odometer.initialize();
  world_clock.initialize();

  /* load some settings */
  int16_t mag_offsets[3];
  settings.get(SettingID::SETTING_MAG_OFFSETS, (uint8_t*)mag_offsets, 3 * sizeof(mag_offsets));
  mag.set_offsets(mag_offsets);
#endif

  /* setup main screens */

  screens[SCREEN_CLOCK]    = new ClockScreen();
#if 0
  screens[SCREEN_SPLASH]   = new SplashScreen();
  screens[SCREEN_SPEED]    = new SpeedScreen();
  screens[SCREEN_BATTERY]  = new BatteryScreen();
  screens[SCREEN_TEMP]     = new TempScreen();
  screens[SCREEN_MAG]      = new MagScreen();
  screens[SCREEN_SUNLIGHT] = new SunlightScreen();
  screens[SCREEN_DATA]     = new DataScreen();
#endif
  screens[SCREEN_SETTINGS] = new SettingsScreen();

  for (int i = 0; i < MAX_SCREENS; i++)
    {
      screens[i]->initialize();
    }

  /* register SIGINT handler */

  setup_sigint();

  /* register defered task handler */

  setup_defered_task();

  /* setup watchdog petting task */

  setup_watchdog_task();

  /* set splash screen to start */

  set_screen(SCREEN_CLOCK);

  /* play wecome tune */
  //buzzer.play(Buzzer::Tune::BOOT);

  return true;
}

void watch::App::setup_watchdog_task(void)
{
  /* We setup a task to be run on every refresh
   * to pet the watchdog ONLY if the button is not pressed.
   * This will detect if LVGL is still processing events which
   * hopfully means it is drawing and sampling input correctly so UI
   * should be functional.
   * If not but LVGL still runs this task, the user can hold the button
   * for the watchdog timeout period and it will eventually reset.
   */

  watchdog_task = lv_task_create_basic();
  lv_task_set_period(watchdog_task, 0);
  lv_task_set_repeat_count(watchdog_task, -1);
  lv_task_set_prio(watchdog_task, LV_TASK_PRIO_LOWEST);
  watchdog_task->user_data = this;

  lv_task_set_cb(watchdog_task, [](lv_task_t* t)
  {
    watch::App* app = (watch::App*)t->user_data;
    printf("watchdog task\n");

    if (!app->input.is_button_pressed())
      {
        /* indicate watchdog pet with vibration motor */

        app->watchdog.pet();
      }
  });

  /* pet now as well */

  if (!input.is_button_pressed())
    {
      watchdog.pet();
    }
}

void watch::App::setup_sigint(void)
{
  /* catch SIGINT signal */

  sigset_t set;
  sigemptyset(&set);
  sigaddset(&set, SIGINT);
  sigprocmask(SIG_UNBLOCK, &set, NULL);

  /* register SIGINT handler */

  struct sigaction act;
  act.sa_sigaction = sigint_handler;
  act.sa_flags     = SA_SIGINFO;
  act.sa_mask      = 0;
  sigaction(SIGINT, &act, NULL);
}

void watch::App::setup_defered_task(void)
{
  /* unblock SIGUSR2 signal */

  sigset_t set;
  sigemptyset(&set);
  sigaddset(&set, SIGUSR2);
  sigprocmask(SIG_UNBLOCK, &set, NULL);

  /* register timeout handler */

  struct sigaction act;
  act.sa_sigaction = defered_task_handler;
  act.sa_mask      = 0;
  act.sa_flags     = SA_SIGINFO;
  sigaction(SIGUSR2, &act, NULL);
}

void watch::App::set_refresh_time(int seconds, const struct timespec& start,
                                  bool absolute)
{
  struct itimerspec t;
  t.it_value = start;
  t.it_interval.tv_sec = seconds;
  t.it_interval.tv_nsec = 0;

  refresh_task.program([](void* arg)
  {
    /* we only care about the signal which breaks the pause() so
     * nothing to do here
     * TODO: we could actually do the tasks of the main loop here but
     * that would mean that other wake ups would not trigger a refresh
     * (button, alarm, etc)
     */

    printf("refresh task\n");

  }, this, t, absolute);
}

void watch::App::task_lock(void)
{
  pthread_mutex_lock(&task_mutex);
}

void watch::App::task_unlock(void)
{
  pthread_mutex_unlock(&task_mutex);
}

void watch::App::sleep(void)
{
#if DEBUG_PRINTS
      printf("sleep\n");
      fflush(stdout);
#endif
  //printf("before pause: %i\n", lv_tick_get());

  if (lv_anim_count_running())
    {
      //display.fast_refresh(true);

      /* only briefly sleep as we're drawing an animation */

      usleep(LV_DISP_DEF_REFR_PERIOD * 1000);

      // TODO: suppor fast mode on here
    }
  else
    {
#if !defined(CONFIG_ARCH_SIM) && !defined(CONFIG_APP_WATCH_NOSLEEP)
      /* relax into sleep */

      struct boardioc_pm_ctrl_s s;
      s.domain = 0;
      s.state = PM_NORMAL;
      s.action = BOARDIOC_PM_RELAX;
      boardctl(BOARDIOC_PM_CONTROL, (uintptr_t)&s);
      s.state = PM_IDLE;
      s.action = BOARDIOC_PM_RELAX;
      boardctl(BOARDIOC_PM_CONTROL, (uintptr_t)&s);
#endif

      /* give control away, until woken up by alarm */

      printf("before pause\n");
      pause();
      printf("after pause\n");

#if !defined(CONFIG_ARCH_SIM) && !defined(CONFIG_APP_WATCH_NOSLEEP)
      /* woke up, now request to stay awake */

      s.state = PM_NORMAL;
      s.action = BOARDIOC_PM_STAY;
      boardctl(BOARDIOC_PM_CONTROL, (uintptr_t)&s);
      s.state = PM_IDLE;
      s.action = BOARDIOC_PM_STAY;
      boardctl(BOARDIOC_PM_CONTROL, (uintptr_t)&s);
#endif
    }

#if DEBUG_PRINTS
      printf("wake\n");
#endif
}

void watch::App::power_management(void)
{
#if !defined(CONFIG_ARCH_SIM) && !defined(CONFIG_APP_WATCH_DISABLE_INACTIVITY)
  /* if no wheel motion is detected recently and we're currently
   * sleeping using STOP1, use STOP2 */

  if (odometer.time_since_moved() > 1)
  {
    uint8_t standbymode;
    boardctl(BOARDIOC_GETSTANDBYMODE, (uintptr_t)&standbymode);
    if (standbymode == 1)
    {
      printf("Set STANDBYMODE: STOP2\n");
      boardctl(BOARDIOC_SETSTANDBYMODE, 2);
    }
  }

  /* check for long inactivity and completely poweroff */

  uint32_t inactive_time = display.inactive_time();
  printf("inactive: %i\n", inactive_time);
  if (inactive_time > 10000 && screen != SCREEN_CLOCK)
  {
    set_screen(SCREEN_CLOCK);
  }
  else if (inactive_time > 60000)
  {
    printf("power off\n");
    fflush(stdout);

    /* disable periodic alarm, will only wakeup on full alarms */

    disable_alarm();

    /* turn of display */

    display.power(false);

    /* go to SHUTDOWN */

    boardctl(BOARDIOC_POWEROFF, 0);
  }
#endif
}

void watch::App::spin(void)
{
  while (!should_stop)
  {
#if DEBUG_PRINTS
    printf("spin\n");
#endif

#if 0
    /* maintain odometer count updated since it is used to detect
     * inactivity and set sleep level */

    odometer.update();
#endif

    /* update current screen */

    update_screen();

    /* process all tasks */

    ScreenID prev_screen = screen;

    watch::g_app->task_lock();
    lv_task_handler();
    watch::g_app->task_unlock();

    /* use lower power modes depending to inactivity */

#ifndef CONFIG_APP_WATCH_DISABLE_INACTIVITY
    power_management();
#endif

    /* Handle screen changes */

    if (prev_screen != screen)
    {
      watch::g_app->task_lock();
      lv_task_handler();
      watch::g_app->task_unlock();
    }

    /* enter low power state and wake up according
     * to current setting of alarm timer or external events */

    sleep();

    /* One more time to handle input immediately before screen update */

    watch::g_app->task_lock();
    lv_task_handler();
    watch::g_app->task_unlock();
  }

  printf("Exiting\n");
}

void watch::App::update_screen(void)
{
  screens[screen]->update();
}

void watch::App::set_next_screen(void)
{
  set_screen((ScreenID)((screen + 1) % MAX_SCREENS));
}

void watch::App::set_prev_screen(void)
{
  set_screen((ScreenID)((screen - 1) % MAX_SCREENS));
}

watch::App::ScreenID watch::App::current_screen(void)
{
  return screen;
}

void watch::App::set_screen(watch::App::ScreenID new_screen)
{
  ScreenID prev_screen = screen;
  screen = new_screen;

  lv_scr_load(screens[screen]->scr);

  screens[prev_screen]->exit(screens[screen]);
  screens[screen]->enter(screens[prev_screen]);

  update_screen();
}
