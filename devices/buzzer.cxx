/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/buzzer.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include "buzzer.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

const char* tunes[] =
{
  /* boot */
  "L64 CDEFGAB",

  /* bip */
  "L128 G",

  /* beep */
  "L8 G"
};

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/


watch::Buzzer::~Buzzer()
{
  if (fd >= 0)
  {
    close(fd);
  }
}

bool watch::Buzzer::initialize()
{
  fd = open("/dev/buzzer", O_WRONLY);

  if (fd < 0)
  {
    perror("Could not open buzzer device");
    return false;
  }
  else
  {
    return true;
  }
}

void watch::Buzzer::play(watch::Buzzer::Tune tune)
{
  write(fd, tunes[(int)tune], strlen(tunes[(int)tune]));
}
