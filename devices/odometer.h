/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/odometer.h
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

#ifndef __ODOMETER_H
#define __ODOMETER_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <sys/time.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Public Types
 ****************************************************************************/

namespace watch  class Odometer
  {
    public:
      Odometer(void) = default;
      ~Odometer(void);

      void update(void);

      void read_counter(void);

      float speed(void);
      float distance(void);

      timeval last_time_moved(void);
      float time_since_moved(void);

      void initialize(void);

      uint32_t last_count = 0, count = 0;
      timeval last_time = { 0, 0 },
              last_motion_time = { 0, 0 },
              time = { 0, 0 };
      float time_delta = 0;
      float time_delta_moved = 0;

      float accumulated_distance = 0;
      float current_speed = 0;

      float wheel_perimeter = 1100; /* [mm] */
  };
}

/****************************************************************************
 * Public Data
 ****************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Inline Functions
 ****************************************************************************/

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif // __ODOMETER_H
