/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/battery.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ************************************************************************************************************/

#include <nuttx/config.h>
#include <nuttx/analog/adc.h>
#include <nuttx/analog/ioctl.h>

#ifndef CONFIG_ARCH_SIM
#include <arch/board/board.h>
#endif

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <fcntl.h>
#include "battery.h"

/************************************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************************************/

#if 0
#define SUPPLY_VOLTAGE (0.6)                 /* ADC reference */
#define GAIN           (6.0)
#else
#define SUPPLY_VOLTAGE (3.3/4.0)
#define GAIN           (4.0)
#endif
#define DIVIDER        (4.245188036718982)
#define MIN_VOLTAGE    3.5
#define MAX_VOLTAGE    4.19

/************************************************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

bool watch::Battery::initialize(void)
{
#ifndef CONFIG_ARCH_SIM
  fd = open("/dev/battery", O_RDONLY);
  if (fd < 0)
    {
      perror("Could not open battery device");
      return false;
    }
  else
    {
      return true;
    }
#endif
  return true;
}

watch::Battery::~Battery(void)
{
#ifndef CONFIG_ARCH_SIM
  if (fd >= 0)
  {
    close(fd);
  }
#endif
}

float watch::Battery::voltage(void)
{
#ifndef CONFIG_ARCH_SIM
  struct adc_msg_s adcm;

  ioctl(fd, ANIOC_TRIGGER, 0);

  int ret = read(fd, &adcm, sizeof(adcm));
  if (ret != sizeof(adcm))
  {
    perror("Could not read ADC");
    return -1.0f;
  }

  return (adcm.am_data / 4096.0) * (GAIN * SUPPLY_VOLTAGE * DIVIDER);
  //return (float)adcm.am_data;
#else
  return 4.22;
#endif
}

float watch::Battery::percent(float v)
{
  float p = (v - MIN_VOLTAGE) / (MAX_VOLTAGE - MIN_VOLTAGE) * 100;

  if (p < 0) return 0;
  else if (p > 100) return 100;
  else return p;
}

watch::Battery::State watch::Battery::state(void)
{
  bool powered = false;
  bool charged = false;

#ifndef CONFIG_ARCH_SIM
  boardctl(BOARDIOC_CHARGED, (uintptr_t)&charged);
  boardctl(BOARDIOC_CHARGING, (uintptr_t)&powered);
#endif

  if (powered)
    {
      if (!charged)
        {
          return State::CHARGING;
        }
      else
        {
          return State::CHARGED;
        }
    }
  else
    {
      return State::DISCHARGING;
    }
}
