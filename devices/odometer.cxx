/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/odometer.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#ifndef CONFIG_ARCH_SIM
#include <arch/board/boardctl.h>
#endif

#include <time.h>
#include <sys/time.h>
#include <stdio.h>
#include "odometer.h"
#include "app.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

static void pulse_handler(int signo, FAR siginfo_t *siginfo,
                          FAR void *context);

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

static void pulse_handler(int signo, FAR siginfo_t *siginfo,
                          FAR void *context)
{
  /* do nothing */
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/


watch::Odometer::~Odometer(void)
{

}

void watch::Odometer::update(void)
{
  /* read counter and update all statistics */

  read_counter();
}

void watch::Odometer::read_counter(void)
{
  last_count = count;
  last_time = time;

  /* read counter */

#ifdef CONFIG_ARCH_SIM
  count = 100;
#else
  boardctl(BOARDIOC_PULSECOUNTER_GET, (uintptr_t)&count);
#endif

  /* compute time delta between last read */

  gettimeofday(&time, nullptr);

  /* compute counter increment */

  int32_t delta_counts = count - last_count;
  //if (delta_counts < 0) delta_counts += INT16_MAX; -> check counter resolution

  /* update distance */

  float delta_distance = delta_counts * wheel_perimeter / 1000.0f; // in meters
  accumulated_distance += delta_distance / 100.0f; /* km */

  /* compute speed */

  if (last_time.tv_sec == 0 && last_time.tv_usec == 0)
  {
    /* not yet valid time */

    current_speed = 0;
  }
  else
  {
    timeval delta;
    timersub(&time, &last_time, &delta);
    time_delta = delta.tv_sec + delta.tv_usec * 1e-6f;

    current_speed = delta_distance / (100.0f * time_delta); /* km/h */
  }

  /* if we detected motion, remember when it happened and how long
   * since it did */

  if (delta_counts > 0)
  {
    printf("moved: %i dist: %f speed: %f\n", delta_counts, accumulated_distance, current_speed);
    last_motion_time = time;
    time_delta_moved = 0;

    watch::g_app->display.report_activity();
  }
  else
  {
    timeval moved_delta;
    timersub(&time, &last_motion_time, &moved_delta);
    time_delta_moved = moved_delta.tv_sec + moved_delta.tv_usec * 1e-6f;
  }
}

float watch::Odometer::speed(void)
{
  return current_speed;
}

float watch::Odometer::distance(void)
{
  return accumulated_distance;
}

timeval watch::Odometer::last_time_moved(void)
{
  return last_motion_time;
}

float watch::Odometer::time_since_moved(void)
{
  return time_delta_moved;
}

void watch::Odometer::initialize(void)
{
#ifndef CONFIG_ARCH_SIM
  /* Request signal generation for pulse on odometer
   * input so that we break the wait
   */

  struct sigevent evt;
  evt.sigev_signo = SIGUSR2;
  evt.sigev_notify = SIGEV_SIGNAL;
  boardctl(BOARDIOC_PULSECOUNTER_NOTIFY, (uintptr_t)&evt);

  /* unblock signal */

  sigset_t set;
  sigemptyset(&set);
  sigaddset(&set, SIGUSR2);
  sigprocmask(SIG_UNBLOCK, &set, NULL);

  /* register alarm handler */

  struct sigaction act;
  act.sa_sigaction = pulse_handler;
  act.sa_flags     = SA_SIGINFO;
  sigaction(SIGUSR2, &act, NULL);
#endif
}
