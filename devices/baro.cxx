/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/baro.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <math.h>
#include <fcntl.h>
#include <string.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <nuttx/sensors/bmp280.h>
#include <nuttx/sensors/ioctl.h>
#include "baro.h"
#include "app.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

bool watch::Baro::initialize(void)
{
  /* open device */

  fd = open("/dev/press0", O_RDONLY);

  if (fd < 0)
  {
    perror("Could not open baro");
    return false;
  }

  /* load data */

  g_app->settings.get(SettingID::SETTING_TEMP_STATS,
                      (uint8_t*)&stats, sizeof(stats));

  /* reset records */

  for (int i = 0; i < CONFIG_LVGL_HOR_RES_MAX; i++)
  {
    temp_history[i] = baro_history[i] = -99.0f;
  }

  return true;
}

watch::Baro::~Baro(void)
{
  if (fd >= 0)
  {
    close(fd);
  }
}

float watch::Baro::get_temperature(void)
{
#ifndef CONFIG_ARCH_SIM
  uint32_t data;

  /* get current temperature */

  ioctl(fd, SNIOC_GET_TEMP, (unsigned long)&data);

  float temp = data / 100.0f;
#else
  float temp = 25;
#endif

  /* update temperature statistics */

  update_stats(temp);

  /* record into temperature history slot */

  record_temperature(temp);

  return temp;
}

float watch::Baro::get_pressure(void)
{
  uint32_t data;

  /* get pressure */

  read(fd, &data, sizeof(data));

  float pressure = data / 100.0f;

  /* record pressure */

  record_pressure(pressure);

  return pressure;
}

float watch::Baro::get_altitude(void)
{
#ifdef CONFIG_ARCH_SIM
  return 0;
#else
  return (1 - pow(get_pressure() / sea_level_pressure, 0.19022256039566293f)) * 44300;
#endif
}

float watch::Baro::compute_sea_level_pressure(float altitude)
{
  return get_pressure() / pow(1 - altitude / 44300, 5.255f);
}

void watch::Baro::set_sea_level_pressure(float pressure)
{
  sea_level_pressure = pressure;
}

watch::Baro::statistics_t watch::Baro::get_statistics(void)
{
  return stats;
}

void watch::Baro::update_stats(float temp)
{
  bool changed = false;

  if (temp < stats.min_temp)
  {
    stats.min_temp = temp;
    changed = true;
  }
  if (temp > stats.max_temp)
  {
    stats.max_temp = temp;
    changed = true;
  }

  /* if stats change, store to EEPROM */

#if 0
  // TODO: check we're not doing this too often

  g_app->settings.set(SettingID::SETTING_TEMP_STATS, sizeof(stats),
                      (uint8_t*)&stats);
#else
  UNUSED(changed);
#endif
}

void watch::Baro::record_temperature(float temp)
{
  time_t now = time(nullptr);
  struct tm* now_tm = localtime(&now);

  int idx = ((now_tm->tm_hour * 60 + now_tm->tm_min) / RECORD_MINUTES_PER_PIXEL);
  temp_history[idx] = temp;
}

void watch::Baro::record_pressure(float pressure)
{
  time_t now = time(nullptr);
  struct tm* now_tm = localtime(&now);

  int idx = ((now_tm->tm_hour * 60 + now_tm->tm_min) / RECORD_MINUTES_PER_PIXEL);
  baro_history[idx] = pressure;
}
