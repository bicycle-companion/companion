/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/devices/clock.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdint.h>
#include <nuttx/timers/rtc.h>
#include "devices/world_clock.h"
#include "app.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define DEG2RAD       (M_PI_F / 180.0f)
#define RAD2DEG       (180.0f / M_PI_F)
#define JULIAN_DAY_0  2451545

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/*
 * The following two functions are based on code found on:
 * https://github.com/mnemocron/libDaylength
 * LICENSE: Apache 2.0
 */

static int days_from_civil(int y, unsigned m, unsigned d)
{
  y -= m <= 2;
  const int era = (y >= 0 ? y : y-399) / 400;
  const unsigned yoe = (unsigned)(y - era * 400);      // [0, 399]
  const unsigned doy = (153*(m + (m > 2 ? -3 : 9)) + 2)/5 + d-1;  // [0, 365]
  const unsigned doe = yoe * 365 + yoe/4 - yoe/100 + doy;         // [0, 146096]
  return era * 146097 + (int)(doe) - 719468;
}

/* Days since Jan 1, 2000 + 2451545 */

static int julian_date(int year, int month, int day)
{
  int days_70_to_00 = days_from_civil(2000, 1, 1) + 1;
  int julian = days_from_civil(year, month, day) - days_70_to_00 + 2451545;
  return julian;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

void watch::WorldClock::set_location(uint16_t _country_idx, uint16_t _city_idx)
{
  country_idx = _country_idx;
  city_idx = _city_idx;

  printf("country: %s city: %s lat: %f long: %f\n", countries[country_idx].name,
         countries[country_idx].cities[city_idx].name,
         countries[country_idx].cities[city_idx].latitude,
         countries[country_idx].cities[city_idx].longitude);
}

void watch::WorldClock::initialize(void)
{
}

void watch::WorldClock::set_alarm(int id, struct tm t, uint8_t mask)
{
#ifndef CONFIG_ARCH_SIM
  struct rtc_setalarm_s alarm;
  alarm.id = id;
  alarm.pid = getpid();
  memcpy(&alarm.time, &t, sizeof(struct tm));
  alarm.timemask = mask;

  ioctl(bc::g_app->rtc_fd, RTC_SET_ALARM, (uintptr_t)&alarm);
#endif
}

/* TODO: a lot of this math could be cached inside the span of a day */

watch::WorldClock::SunTimes watch::WorldClock::compute_suntimes(const struct tm* t)
{
  float lat = countries[country_idx].cities[city_idx].latitude;
  float lon = countries[country_idx].cities[city_idx].longitude;
  float tz = countries[country_idx].cities[city_idx].timezone;

  /* These two variables are not as precise as computed in solar spreadsheet math,
   * but they generate up to 10s error, so no big deal. The 0.5 comes from taking the
   * measurement from around middle of day to compensate a bit for this error
   */
  float jdate = julian_date(1900 + t->tm_year, t->tm_mon + 1, t->tm_mday) - tz / 24.0 + 0.5;
  float julian_century = (jdate - JULIAN_DAY_0) / 36525;

  float geom_long = fmodf(280.46646f + julian_century * (36000.76983f + julian_century * 0.0003032f), 360.0f);
  float geom_anom = 357.52911f + julian_century * (35999.05029f - 0.0001537f * julian_century);

  float sun_eq_of_ctr = sinf(DEG2RAD * geom_anom) * (1.914602f - julian_century * (0.004817f + 0.000014f * julian_century)) + sinf(DEG2RAD * (2 * geom_anom)) * (0.019993f - 0.000101f * julian_century) + sinf(DEG2RAD * (3 * geom_anom)) * 0.000289f;
  float sun_true_long = geom_long + sun_eq_of_ctr;
  float sun_app_long = sun_true_long - 0.00569f - 0.00478 * sinf(DEG2RAD * (125.04f - 1934.136f * julian_century));
  float eccent_orbit = 0.016708634f - julian_century * (0.000042037f + 0.0000001267f * julian_century);
  float mean_obliq_eliptic = 23 + (26 + ((21.448f - julian_century * (46.815f + julian_century * (0.00059 - julian_century * 0.001813f)))) / 60) / 60;
  float obliq_corr = mean_obliq_eliptic + 0.00256 * cosf(DEG2RAD * (125.04f - 1934.136 * julian_century));
  float var_y = tanf(DEG2RAD * (obliq_corr / 2)) * tanf(DEG2RAD * (obliq_corr / 2));
  float eq_of_time = 4 * RAD2DEG * (var_y * sinf(2 * DEG2RAD * geom_long) - 2 * eccent_orbit * sinf(DEG2RAD * geom_anom)+ 4 * eccent_orbit * var_y * sinf(DEG2RAD * geom_anom) * cosf(2 * DEG2RAD * geom_long) - 0.5f * var_y * var_y * sinf(4 * DEG2RAD * geom_long) - 1.25f * eccent_orbit * eccent_orbit * sinf(2 * DEG2RAD * geom_anom));
  float solar_noon = (720 - 4 * lon - eq_of_time + tz * 60) / 1440;
  float sun_declin = RAD2DEG * (asinf(sinf(DEG2RAD * obliq_corr) * sinf(DEG2RAD * sun_app_long)));
  float ha_sunrise = RAD2DEG * (acosf(cosf(DEG2RAD * 90.833f) / (cosf(DEG2RAD * lat) * cosf(DEG2RAD * sun_declin)) - tanf(DEG2RAD * lat) * tanf(DEG2RAD * sun_declin)));
  float sunrise = solar_noon - ha_sunrise * 4 / 1440.0f; /* seconds */
  float sunset = solar_noon + ha_sunrise * 4 / 1440.0f; /* seconds */
  float sunlight = 8 * ha_sunrise; /* minutes */

  SunTimes st;
  st.sunset = (int)(sunset * 3600 * 24);
  st.sunrise = (int)(sunrise * 3600 * 24);
  st.sunlight = (int)sunlight * 60;

#if 0
  printf("sunrise:\t%i:%i:%i\n", (int)(st.sunrise / 3600), (int)((st.sunrise % 3600) / 60), st.sunrise % 60);
  printf("sunset:\t%i:%i:%i\n", (int)(st.sunset / 3600), (int)((st.sunset % 3600) / 60), st.sunset % 60);
  printf("sunlight: %i\n", st.sunlight);
#endif
  return st;
}

watch::WorldClock::SunTimes watch::WorldClock::suntimes(void)
{
  time_t now = time(nullptr);
  struct tm* t = localtime(&now);

  return compute_suntimes(t);
}
