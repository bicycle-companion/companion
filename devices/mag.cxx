/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/mag.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <mc6470/mc6470_mag.h>
#include <nuttx/sensors/ioctl.h>
#include <stdio.h>
#include <fcntl.h>
#include <math.h>
#include <sys/ioctl.h>
#include "mag.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

watch::Mag::~Mag(void)
{
  if (fd >= 0)
  {
    close(fd);
  }
}

bool watch::Mag::initialize(void)
{
  fd = open("/dev/mag", O_RDONLY);

  if (fd < 0)
  {
    perror("Could not open magnetometer");
    return false;
  }
  else
  {
    return true;
  }
}

void watch::Mag::enable(bool on)
{
  int ret = ioctl(fd, (on ? SNIOC_ENABLE : SNIOC_DISABLE), 0);

  if (ret < 0)
  {
    perror("Could not enable magnetometer");
  }
}

void watch::Mag::continuous_measurement(bool enable)
{
  int ret = ioctl(fd, SNIOC_SETMODE, enable);

  if (ret < 0)
  {
    perror("Could not set continuous measurement mode");
  }
}

void watch::Mag::calibrate(void)
{
  const int num_samples = 300; /* 30s of samples at 10Hz */
  int32_t sum[3] = { 0, 0, 0 };
  int16_t offset[3] = { 0, 0, 0 };

  /* reset offsets to zero before calibration */

  set_offsets(offset);

  /* set to continuous mode, so we poll at sensor rate */

  ioctl(fd, SNIOC_SETMODE, true);

  /* start sampling at 10Hz (sensor should be rotated now) */

  for (int i = 0; i < num_samples; i++)
  {
    int16_t rawdata[3];
    get_rawdata(rawdata);
    printf("sample %i: %i %i %i\n", i, rawdata[0], rawdata[1], rawdata[2]);
    for (int j = 0; j < 3; j++) { sum[j] += rawdata[j]; }
  }

  for (int i = 0; i < 3; i++)
  {
    offset[i] = (int16_t)(sum[i] / num_samples);
  }

  /* write offsets to device */

  printf("mag offsets: %i %i %i\n", offset[0], offset[1], offset[2]);

  set_offsets(offset);

  /* disable continuous mode */

  ioctl(fd, SNIOC_SETMODE, false);
}

void watch::Mag::get_offsets(int16_t* data)
{
  ioctl(fd, SNIOC_GETOFFSET, (unsigned long)data);
}

void watch::Mag::set_offsets(int16_t* data)
{
  printf("setting offsets: %i %i %i\n", data[0], data[1], data[2]);
  ioctl(fd, SNIOC_SETOFFSET, (unsigned long)data);
}

void watch::Mag::get_rawdata(int16_t* data)
{
  read(fd, (char*)data, sizeof(data));
}

void watch::Mag::get_field(float* data)
{
  int16_t rawdata[3];
  get_rawdata(rawdata);

  for (int i = 0; i < 3; i++)
  {
    /* signed 14bit data */

    data[i] = (rawdata[i] / (float)((1 << 13) - 1));
  }
}

float watch::Mag::get_orientation(float roll, float pitch)
{
#ifdef CONFIG_ARCH_SIM
  time_t now = time(nullptr);
  struct tm* t = localtime(&now);
  return ((t->tm_min % 6) * 60 + t->tm_sec) / 180.0f * M_PI_F;
#else
  // TODO: compensate for hard-iron / soft-iron distortions

  float mag[3];
  get_field(mag);

  float rolls = sinf(roll);
  float rollc = cosf(roll);
  float pitchs = sinf(pitch);
  float pitchc = cosf(pitch);

  float yaw = atan2f(
                mag[2] * rolls - mag[1] * rollc,
                mag[0] * pitchc +
                mag[1] * pitchs * rolls + mag[2] * pitchs * rollc);

  if (yaw < 0)
  {
    yaw = 2 * M_PI_F + yaw;
  }

  return yaw;
#endif
}
