/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/mag.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <mc6470/mc6470_acc.h>
#include <nuttx/sensors/ioctl.h>
#include <stdio.h>
#include <fcntl.h>
#include <math.h>
#include <sys/ioctl.h>
#include "acc.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

watch::Acc::~Acc(void)
{
  if (fd >= 0)
  {
    close(fd);
  }
}

bool watch::Acc::initialize(void)
{
  fd = open("/dev/acc", O_RDONLY);

  if (fd < 0)
    {
      perror("Could not open accelerometer");
      return false;
    }
  else
    {
      return true;
    }
}

void watch::Acc::enable(bool on)
{
  int ret = ioctl(fd, (on ? SNIOC_ENABLE : SNIOC_DISABLE), 0);

  if (ret < 0)
    {
      perror("Could not enable magnetometer");
    }
}

void watch::Acc::tap_detect(bool on)
{
  if (on)
    {
      /* configure tap detection */

      struct mc6470_acc_tap_settings_s tapsettings;
      tapsettings.threshold[0] = 0x00;
      tapsettings.threshold[1] = 0x00;
      tapsettings.threshold[2] = 0x00;
      tapsettings.tap_detect = MC6470_TAP_BY_THRESHOLD | MC6470_TAP_X_POS |
                               MC6470_TAP_Y_POS | MC6470_TAP_Z_POS;

      int ret = ioctl(fd, SNIOC_TAP_CONFIGURE, (unsigned long)&tapsettings);
      if (ret < 0)
        {
          perror("Could not configure tap settings");
          return;
        }
    }

  int ret = ioctl(fd, SNIOC_TAP_DETECT, on);
  if (ret < 0)
    {
      perror("Could not enable tap detection");
      return;
    }
}

void watch::Acc::get_acceleration(float* data)
{
  int16_t rawdata[3];
  get_rawdata(rawdata);

  for (int i = 0; i < 3; i++)
    {
      /* 16g, 14bit (signed) assumed */

      data[i] = (rawdata[i] / (float)((1 << 13) - 1)) * 16 * 9.8f;
    }
}

void watch::Acc::get_orientation(float& roll, float& pitch)
{
#ifdef CONFIG_ARCH_SIM
  roll = pitch = 0;
#else
  float acc[3];
  get_acceleration(acc);

  /* use atan2 for [-180,180] solution */
  roll = atan2(acc[1], acc[2]);

  /* use atan for [-90,90] solution */
  pitch = atanf(-acc[0] / (acc[1] * sinf(roll) + acc[2] * cosf(roll)));
#endif
}

void watch::Acc::get_rawdata(int16_t* data)
{
  struct mc6470_acc_data_s rawdata;

  read(fd, &rawdata, sizeof(rawdata));

  for (int i = 0; i < 3; i++)
    {
      data[i] = rawdata.acc[i];
    }

  printf("bits: %i\n", rawdata.tap_state);
}
