/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/devices/clock.h
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

#ifndef __CLOCK_H
#define __CLOCK_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdlib.h>

struct city_t
{
  const char* name;
  float latitude, longitude;
  int8_t timezone;   /* offset from GMT */
};

struct country_t
{
  const char* name;
  const city_t* cities;
};

const city_t argentine_cities[] =
{
  {
    .name = "Buenos Aires",
    .latitude = -34.6037f,
    .longitude = -58.3816f,
    .timezone = -3,
  },
};

const country_t countries[] =
{
  {
    .name = "Argentina",
    .cities = argentine_cities
  }
};

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Public Types
 ****************************************************************************/

namespace watch  class WorldClock
  {
    public:
      WorldClock(void) = default;

      void set_location(uint16_t country_idx, uint16_t city_idx);

      struct SunTimes
      {
        uint32_t sunlight; /* minutes */
        uint32_t sunrise; /* seconds from 00:00 */
        uint32_t sunset; /* seconds from 00:00 */
        uint32_t noon; /* seconds from 00:00 */
      };

      SunTimes suntimes(void);

      void initialize(void);

      void set_alarm(int id, struct tm t, uint8_t mask);

    private:
      uint16_t country_idx = 0, city_idx = 0;

      SunTimes compute_suntimes(const tm* t);
  };
}


/****************************************************************************
 * Public Data
 ****************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Inline Functions
 ****************************************************************************/

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif // __CLOCK_H
