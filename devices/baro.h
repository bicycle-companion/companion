/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/baro.h
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

#ifndef __BARO_H
#define __BARO_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <float.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define RECORD_MINUTES_PER_PIXEL  15  /* 1 pixel = 15m in the records */

/****************************************************************************
 * Public Types
 ****************************************************************************/

namespace watch  class Baro
    {
      public:

        struct statistics_t
        {
          float min_temp = FLT_MAX, max_temp = FLT_MIN;
        } stats;

        Baro(void) = default;
        ~Baro(void);

        bool initialize(void);

        float get_temperature(void);
        float get_pressure(void);
        float get_altitude(void);

        float compute_sea_level_pressure(float altitude);

        void set_sea_level_pressure(float pressure);

        statistics_t get_statistics(void);

        float temp_history[CONFIG_LVGL_HOR_RES_MAX];
        float baro_history[CONFIG_LVGL_HOR_RES_MAX];

      private:
        int fd = -1;

        float sea_level_pressure = 1013.25;

        void update_stats(float temp);
        void record_temperature(float temp);
        void record_pressure(float pressure);
    };
}

/****************************************************************************
 * Public Data
 ****************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Inline Functions
 ****************************************************************************/

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif // __BARO_H
