#include <nuttx/config.h>
#include <lvgl/lvgl.h>

/*******************************************************************************
 * Size: 14 px
 * Bpp: 1
 * Opts: --bpp 1 --size 14 --font /home/v01d/Downloads/fonts/Koleeko.ttf --symbols  -_.,:!?°= --output ../extra_apps/bicycle_companion/fonts/koleeko14.c --format lvgl --autohint-off -r 48-58 -r 65-90 -r 97-122
 ******************************************************************************/

#ifndef KOLEEKO14
#define KOLEEKO14 1
#endif

#if KOLEEKO14

/*-----------------
 *    BITMAPS
 *----------------*/

/*Store the image of the glyphs*/
static LV_ATTRIBUTE_LARGE_CONST const uint8_t gylph_bitmap[] = {
    /* U+20 " " */
    0x0,

    /* U+21 "!" */
    0xff, 0xfe, 0x3f,

    /* U+2C "," */
    0x77, 0x7e,

    /* U+2D "-" */
    0xf0,

    /* U+2E "." */
    0xff, 0x80,

    /* U+30 "0" */
    0x77, 0xf7, 0xbd, 0xef, 0xee,

    /* U+31 "1" */
    0x2f, 0xf6, 0xdb,

    /* U+32 "2" */
    0x77, 0xc6, 0x36, 0x73, 0xff,

    /* U+33 "3" */
    0x77, 0xc6, 0xf7, 0x8f, 0xee,

    /* U+34 "4" */
    0x18, 0x63, 0x9e, 0xdf, 0xf1, 0x86,

    /* U+35 "5" */
    0xff, 0xf1, 0x8f, 0x8f, 0xfe,

    /* U+36 "6" */
    0x33, 0xb1, 0x8d, 0xef, 0xee,

    /* U+37 "7" */
    0xff, 0xf1, 0x84, 0x31, 0xc7, 0x1c,

    /* U+38 "8" */
    0x77, 0xf7, 0xb5, 0xef, 0xee,

    /* U+39 "9" */
    0x77, 0xf7, 0xb7, 0x8d, 0xce,

    /* U+3A ":" */
    0xff, 0xff, 0xc0,

    /* U+3D "=" */
    0xff, 0xff,

    /* U+3F "?" */
    0xfb, 0xff, 0xc7, 0x30, 0x7, 0x1c,

    /* U+41 "A" */
    0x7b, 0xff, 0xfb, 0xef, 0xfd, 0xf7,

    /* U+42 "B" */
    0xfb, 0xfe, 0xfe, 0xef, 0xff, 0xfe,

    /* U+43 "C" */
    0x7b, 0xfe, 0xf8, 0xef, 0xff, 0xde,

    /* U+44 "D" */
    0xf3, 0xef, 0xfb, 0xef, 0xff, 0xfe,

    /* U+45 "E" */
    0xff, 0xfe, 0x3e, 0xe3, 0xff, 0xff,

    /* U+46 "F" */
    0xff, 0xfe, 0x3e, 0xe3, 0x8e, 0x38,

    /* U+47 "G" */
    0x7b, 0xfe, 0x3f, 0xff, 0xbf, 0xde,

    /* U+48 "H" */
    0xdf, 0x7d, 0xff, 0xff, 0xbe, 0xfb,

    /* U+49 "I" */
    0xff, 0x77, 0x7f, 0xff,

    /* U+4A "J" */
    0x7b, 0xcc, 0x63, 0x7b, 0xde,

    /* U+4B "K" */
    0xef, 0xbe, 0xfe, 0xfb, 0x7d, 0xf7,

    /* U+4C "L" */
    0xe7, 0x39, 0xcf, 0xff, 0xff,

    /* U+4D "M" */
    0x66, 0xff, 0xff, 0xff, 0xf7, 0xf7, 0xe7, 0xe7,

    /* U+4E "N" */
    0xef, 0xff, 0xff, 0xdf, 0x7d, 0xf7,

    /* U+4F "O" */
    0x7b, 0xfe, 0xfb, 0xef, 0xff, 0xde,

    /* U+50 "P" */
    0xf3, 0xef, 0xfb, 0xeb, 0xee, 0x38,

    /* U+51 "Q" */
    0x7b, 0xfe, 0xfb, 0xef, 0xef, 0xdf,

    /* U+52 "R" */
    0xf3, 0xef, 0xfb, 0xeb, 0xfd, 0xf3,

    /* U+53 "S" */
    0x7b, 0xff, 0xf8, 0x7f, 0xff, 0xfe,

    /* U+54 "T" */
    0xff, 0xff, 0xcc, 0x30, 0xc3, 0xc,

    /* U+55 "U" */
    0xef, 0xbe, 0xfb, 0xef, 0xff, 0xde,

    /* U+56 "V" */
    0xef, 0xbe, 0xfb, 0xed, 0xe3, 0x4,

    /* U+57 "W" */
    0xec, 0xfb, 0x3e, 0xcf, 0xb3, 0xec, 0xdf, 0xe3,
    0x70, 0x48,

    /* U+58 "X" */
    0xef, 0xbf, 0xcc, 0x7f, 0x7d, 0xf7,

    /* U+59 "Y" */
    0xef, 0xbe, 0xfb, 0xf8, 0xc3, 0xc,

    /* U+5A "Z" */
    0xff, 0xff, 0xc6, 0x33, 0xff, 0xff,

    /* U+5F "_" */
    0xff, 0xf0,

    /* U+61 "a" */
    0x7b, 0xf0, 0xd7, 0xdf, 0xf7, 0xc0,

    /* U+62 "b" */
    0xe3, 0x8f, 0xbf, 0xef, 0xbe, 0xff, 0xf8,

    /* U+63 "c" */
    0x7b, 0xfe, 0x38, 0xe3, 0xf7, 0x80,

    /* U+64 "d" */
    0xc, 0x37, 0xff, 0xef, 0xbe, 0xff, 0x7c,

    /* U+65 "e" */
    0x7b, 0xfe, 0xf8, 0xe3, 0xf7, 0xc0,

    /* U+66 "f" */
    0x1c, 0xf7, 0x1c, 0xfb, 0xc7, 0x1c, 0x70,

    /* U+67 "g" */
    0x7f, 0xfe, 0xfb, 0xff, 0xf7, 0xc3, 0x7d, 0xe0,

    /* U+68 "h" */
    0xe3, 0x8f, 0xbf, 0xef, 0xbe, 0xfb, 0xec,

    /* U+69 "i" */
    0xfc, 0x7f, 0xff,

    /* U+6A "j" */
    0x33, 0x3, 0x33, 0x33, 0x3f, 0xe0,

    /* U+6B "k" */
    0xe3, 0x8e, 0xfb, 0xff, 0xfe, 0xfb, 0xec,

    /* U+6C "l" */
    0xff, 0xff, 0xff,

    /* U+6D "m" */
    0xf6, 0xff, 0xff, 0xfb, 0xfb, 0xfb, 0xfb,

    /* U+6E "n" */
    0x7b, 0xfe, 0xfb, 0xef, 0xbe, 0xc0,

    /* U+6F "o" */
    0x7b, 0xfe, 0xfb, 0xef, 0xf7, 0x80,

    /* U+70 "p" */
    0xfb, 0xfe, 0xfb, 0xef, 0xff, 0xb8, 0xe0,

    /* U+71 "q" */
    0x7f, 0xfe, 0xfb, 0xef, 0xf7, 0xc7, 0x1c,

    /* U+72 "r" */
    0x7b, 0xfe, 0x38, 0xe3, 0x8e, 0x0,

    /* U+73 "s" */
    0x7f, 0xfe, 0x1f, 0x1f, 0xff, 0x80,

    /* U+74 "t" */
    0x77, 0xff, 0x77, 0x77, 0x30,

    /* U+75 "u" */
    0xef, 0xbe, 0xfb, 0xef, 0xf7, 0x80,

    /* U+76 "v" */
    0xef, 0xbe, 0xfb, 0xed, 0xe3, 0x0,

    /* U+77 "w" */
    0xe3, 0xfb, 0xfb, 0xfb, 0xfb, 0x7e, 0x3c,

    /* U+78 "x" */
    0xef, 0xbf, 0xce, 0xff, 0x7d, 0xc0,

    /* U+79 "y" */
    0xdf, 0x7d, 0xf7, 0xdf, 0xf7, 0xc7, 0xfb, 0xc0,

    /* U+7A "z" */
    0xff, 0xf1, 0xc4, 0x73, 0xff, 0xc0
};


/*---------------------
 *  GLYPH DESCRIPTION
 *--------------------*/

static const lv_font_fmt_txt_glyph_dsc_t glyph_dsc[] = {
    {.bitmap_index = 0, .adv_w = 0, .box_w = 0, .box_h = 0, .ofs_x = 0, .ofs_y = 0} /* id = 0 reserved */,
    {.bitmap_index = 0, .adv_w = 70, .box_w = 1, .box_h = 1, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1, .adv_w = 56, .box_w = 3, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 4, .adv_w = 70, .box_w = 4, .box_h = 4, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 6, .adv_w = 84, .box_w = 4, .box_h = 1, .ofs_x = 0, .ofs_y = 3},
    {.bitmap_index = 7, .adv_w = 56, .box_w = 3, .box_h = 3, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 9, .adv_w = 98, .box_w = 5, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 14, .adv_w = 56, .box_w = 3, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 17, .adv_w = 98, .box_w = 5, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 22, .adv_w = 98, .box_w = 5, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 27, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 33, .adv_w = 98, .box_w = 5, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 38, .adv_w = 98, .box_w = 5, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 43, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 49, .adv_w = 98, .box_w = 5, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 54, .adv_w = 98, .box_w = 5, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 59, .adv_w = 56, .box_w = 3, .box_h = 6, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 62, .adv_w = 84, .box_w = 4, .box_h = 4, .ofs_x = 0, .ofs_y = 2},
    {.bitmap_index = 64, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 70, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 76, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 82, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 88, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 94, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 100, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 106, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 112, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 118, .adv_w = 84, .box_w = 4, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 122, .adv_w = 98, .box_w = 5, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 127, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 133, .adv_w = 98, .box_w = 5, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 138, .adv_w = 140, .box_w = 8, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 146, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 152, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 158, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 164, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 170, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 176, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 182, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 188, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 194, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 200, .adv_w = 168, .box_w = 10, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 210, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 216, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 222, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 228, .adv_w = 112, .box_w = 6, .box_h = 2, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 230, .adv_w = 112, .box_w = 6, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 236, .adv_w = 112, .box_w = 6, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 243, .adv_w = 112, .box_w = 6, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 249, .adv_w = 112, .box_w = 6, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 256, .adv_w = 112, .box_w = 6, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 262, .adv_w = 112, .box_w = 6, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 269, .adv_w = 112, .box_w = 6, .box_h = 10, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 277, .adv_w = 112, .box_w = 6, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 284, .adv_w = 56, .box_w = 3, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 287, .adv_w = 84, .box_w = 4, .box_h = 11, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 293, .adv_w = 112, .box_w = 6, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 300, .adv_w = 56, .box_w = 3, .box_h = 8, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 303, .adv_w = 140, .box_w = 8, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 310, .adv_w = 112, .box_w = 6, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 316, .adv_w = 112, .box_w = 6, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 322, .adv_w = 112, .box_w = 6, .box_h = 9, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 329, .adv_w = 112, .box_w = 6, .box_h = 9, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 336, .adv_w = 112, .box_w = 6, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 342, .adv_w = 112, .box_w = 6, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 348, .adv_w = 84, .box_w = 4, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 353, .adv_w = 112, .box_w = 6, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 359, .adv_w = 112, .box_w = 6, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 365, .adv_w = 140, .box_w = 8, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 372, .adv_w = 112, .box_w = 6, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 378, .adv_w = 112, .box_w = 6, .box_h = 10, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 386, .adv_w = 112, .box_w = 6, .box_h = 7, .ofs_x = 0, .ofs_y = 0}
};

/*---------------------
 *  CHARACTER MAPPING
 *--------------------*/

static const uint8_t glyph_id_ofs_list_0[] = {
    0, 1, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 2, 3, 4, 0,
    5, 6, 7, 8, 9, 10, 11, 12,
    13, 14, 15, 0, 0, 16, 0, 17,
    0, 18, 19, 20, 21, 22, 23, 24,
    25, 26, 27, 28, 29, 30, 31, 32,
    33, 34, 35, 36, 37, 38, 39, 40,
    41, 42, 43, 0, 0, 0, 0, 44
};

/*Collect the unicode lists and glyph_id offsets*/
static const lv_font_fmt_txt_cmap_t cmaps[] =
{
    {
        .range_start = 32, .range_length = 64, .glyph_id_start = 1,
        .unicode_list = NULL, .glyph_id_ofs_list = glyph_id_ofs_list_0, .list_length = 64, .type = LV_FONT_FMT_TXT_CMAP_FORMAT0_FULL
    },
    {
        .range_start = 97, .range_length = 26, .glyph_id_start = 46,
        .unicode_list = NULL, .glyph_id_ofs_list = NULL, .list_length = 0, .type = LV_FONT_FMT_TXT_CMAP_FORMAT0_TINY
    }
};



/*--------------------
 *  ALL CUSTOM DATA
 *--------------------*/

/*Store all the custom data of the font*/
static lv_font_fmt_txt_dsc_t font_dsc = {
    .glyph_bitmap = gylph_bitmap,
    .glyph_dsc = glyph_dsc,
    .cmaps = cmaps,
    .kern_dsc = NULL,
    .kern_scale = 0,
    .cmap_num = 2,
    .bpp = 1,
    .kern_classes = 0,
    .bitmap_format = 0
};


/*-----------------
 *  PUBLIC FONT
 *----------------*/

/*Initialize a public general font descriptor*/
lv_font_t g_font_koleeko14 = {
    .get_glyph_dsc = lv_font_get_glyph_dsc_fmt_txt,    /*Function pointer to get glyph's data*/
    .get_glyph_bitmap = lv_font_get_bitmap_fmt_txt,    /*Function pointer to get glyph's bitmap*/
    .line_height = 12,          /*The maximum line height required by the font*/
    .base_line = 3,             /*Baseline measured from the bottom of the line*/
#if !(LVGL_VERSION_MAJOR == 6 && LVGL_VERSION_MINOR == 0)
    .subpx = LV_FONT_SUBPX_NONE,
#endif
    .dsc = &font_dsc           /*The custom font data. Will be accessed by `get_glyph_bitmap/dsc` */
};

#endif /*#if KOLEEKO14*/

