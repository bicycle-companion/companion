#include <nuttx/config.h>
#include <lvgl/lvgl.h>

/*******************************************************************************
 * Size: 8 px
 * Bpp: 1
 * Opts: --bpp 1 --size 8 --font /home/v01d/Downloads/fonts/nokiafc22.ttf --symbols  -_.,:!?°= --output ../extra_apps/bicycle_companion/fonts/nokia8.c --format lvgl --autohint-off -r 48-58 -r 65-90 -r 97-122
 ******************************************************************************/

#ifndef NOKIA8
#define NOKIA8 1
#endif

#if NOKIA8

/*-----------------
 *    BITMAPS
 *----------------*/

/*Store the image of the glyphs*/
static LV_ATTRIBUTE_LARGE_CONST const uint8_t gylph_bitmap[] = {
    /* U+20 " " */
    0x0,

    /* U+21 "!" */
    0xff, 0xcc,

    /* U+2C "," */
    0x78,

    /* U+2D "-" */
    0xf0,

    /* U+2E "." */
    0xf0,

    /* U+30 "0" */
    0x76, 0xf7, 0xbd, 0xed, 0xc0,

    /* U+31 "1" */
    0x7d, 0xb6, 0xd8,

    /* U+32 "2" */
    0xf0, 0xc6, 0xec, 0x63, 0xe0,

    /* U+33 "3" */
    0xf0, 0xc6, 0xe1, 0x8f, 0xc0,

    /* U+34 "4" */
    0x19, 0xd7, 0x3f, 0x8c, 0x60,

    /* U+35 "5" */
    0xf4, 0x3c, 0x31, 0x8f, 0xc0,

    /* U+36 "6" */
    0x76, 0x3d, 0xbd, 0xed, 0xc0,

    /* U+37 "7" */
    0xf8, 0xcc, 0x66, 0x31, 0x80,

    /* U+38 "8" */
    0x76, 0xf6, 0xed, 0xed, 0xc0,

    /* U+39 "9" */
    0x76, 0xf7, 0xb7, 0x8d, 0xc0,

    /* U+3A ":" */
    0xf3, 0xc0,

    /* U+3D "=" */
    0xf0, 0xf0,

    /* U+3F "?" */
    0xf0, 0xcc, 0xc6, 0x1, 0x80,

    /* U+41 "A" */
    0x76, 0xf7, 0xbf, 0xef, 0x60,

    /* U+42 "B" */
    0xf6, 0xfd, 0xbd, 0xef, 0xc0,

    /* U+43 "C" */
    0x7e, 0x31, 0x8c, 0x61, 0xe0,

    /* U+44 "D" */
    0xf6, 0xf7, 0xbd, 0xef, 0xc0,

    /* U+45 "E" */
    0xfe, 0x3d, 0x8c, 0x63, 0xe0,

    /* U+46 "F" */
    0xfe, 0x3d, 0x8c, 0x63, 0x0,

    /* U+47 "G" */
    0x76, 0x31, 0xbd, 0xed, 0xe0,

    /* U+48 "H" */
    0xde, 0xff, 0xbd, 0xef, 0x60,

    /* U+49 "I" */
    0xff, 0xfc,

    /* U+4A "J" */
    0x33, 0x33, 0x33, 0xe0,

    /* U+4B "K" */
    0xcf, 0x6f, 0x38, 0xf3, 0x6c, 0xc0,

    /* U+4C "L" */
    0xcc, 0xcc, 0xcc, 0xf0,

    /* U+4D "M" */
    0x83, 0x8f, 0xbf, 0xfd, 0x78, 0xf1, 0x80,

    /* U+4E "N" */
    0x8f, 0x3e, 0xff, 0xdf, 0x3c, 0x40,

    /* U+4F "O" */
    0x7b, 0x3c, 0xf3, 0xcf, 0x37, 0x80,

    /* U+50 "P" */
    0xf6, 0xf7, 0xbf, 0x63, 0x0,

    /* U+51 "Q" */
    0x7b, 0x3c, 0xf3, 0xcf, 0x77, 0x83,

    /* U+52 "R" */
    0xf6, 0xf7, 0xbf, 0x6b, 0x60,

    /* U+53 "S" */
    0x7c, 0xc6, 0x33, 0xe0,

    /* U+54 "T" */
    0xfc, 0xc3, 0xc, 0x30, 0xc3, 0x0,

    /* U+55 "U" */
    0xde, 0xf7, 0xbd, 0xed, 0xc0,

    /* U+56 "V" */
    0xcf, 0x3c, 0xde, 0x78, 0xc3, 0x0,

    /* U+57 "W" */
    0xc7, 0x8f, 0x5f, 0xf7, 0xcf, 0x9b, 0x0,

    /* U+58 "X" */
    0xcf, 0x37, 0x8c, 0x7b, 0x3c, 0xc0,

    /* U+59 "Y" */
    0xcf, 0x37, 0x8c, 0x30, 0xc3, 0x0,

    /* U+5A "Z" */
    0xf8, 0xce, 0xee, 0x63, 0xe0,

    /* U+5F "_" */
    0xfc,

    /* U+61 "a" */
    0x70, 0xdf, 0xb7, 0x80,

    /* U+62 "b" */
    0xc6, 0x3d, 0xbd, 0xef, 0xc0,

    /* U+63 "c" */
    0x7c, 0xcc, 0x70,

    /* U+64 "d" */
    0x18, 0xdf, 0xbd, 0xed, 0xe0,

    /* U+65 "e" */
    0x76, 0xff, 0x87, 0x80,

    /* U+66 "f" */
    0x7b, 0xed, 0xb0,

    /* U+67 "g" */
    0x7e, 0xf6, 0xf1, 0xb8,

    /* U+68 "h" */
    0xc6, 0x3d, 0xbd, 0xef, 0x60,

    /* U+69 "i" */
    0xcf, 0xfc,

    /* U+6A "j" */
    0x61, 0xb6, 0xde,

    /* U+6B "k" */
    0xc6, 0x37, 0xee, 0x7b, 0x60,

    /* U+6C "l" */
    0xff, 0xfc,

    /* U+6D "m" */
    0xfe, 0xdb, 0xdb, 0xdb, 0xdb,

    /* U+6E "n" */
    0xf6, 0xf7, 0xbd, 0x80,

    /* U+6F "o" */
    0x76, 0xf7, 0xb7, 0x0,

    /* U+70 "p" */
    0xf6, 0xf7, 0xec, 0x60,

    /* U+71 "q" */
    0x7e, 0xf6, 0xf1, 0x8c,

    /* U+72 "r" */
    0xdf, 0xcc, 0xc0,

    /* U+73 "s" */
    0x7c, 0xf3, 0xe0,

    /* U+74 "t" */
    0xdb, 0xed, 0x98,

    /* U+75 "u" */
    0xde, 0xf7, 0xb7, 0x80,

    /* U+76 "v" */
    0xde, 0xdc, 0xe2, 0x0,

    /* U+77 "w" */
    0xc7, 0xaf, 0x5b, 0xe6, 0xc0,

    /* U+78 "x" */
    0xde, 0xdd, 0xbd, 0x80,

    /* U+79 "y" */
    0xde, 0xf6, 0xf1, 0xb8,

    /* U+7A "z" */
    0xf9, 0x99, 0x8f, 0x80
};


/*---------------------
 *  GLYPH DESCRIPTION
 *--------------------*/

static const lv_font_fmt_txt_glyph_dsc_t glyph_dsc[] = {
    {.bitmap_index = 0, .adv_w = 0, .box_w = 0, .box_h = 0, .ofs_x = 0, .ofs_y = 0} /* id = 0 reserved */,
    {.bitmap_index = 0, .adv_w = 48, .box_w = 1, .box_h = 1, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1, .adv_w = 48, .box_w = 2, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 3, .adv_w = 48, .box_w = 2, .box_h = 3, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 4, .adv_w = 80, .box_w = 4, .box_h = 1, .ofs_x = 0, .ofs_y = 3},
    {.bitmap_index = 5, .adv_w = 48, .box_w = 2, .box_h = 2, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 6, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 11, .adv_w = 64, .box_w = 3, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 14, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 19, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 24, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 29, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 34, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 39, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 44, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 49, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 54, .adv_w = 48, .box_w = 2, .box_h = 5, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 56, .adv_w = 80, .box_w = 4, .box_h = 3, .ofs_x = 0, .ofs_y = 1},
    {.bitmap_index = 58, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 63, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 68, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 73, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 78, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 83, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 88, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 93, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 98, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 103, .adv_w = 48, .box_w = 2, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 105, .adv_w = 80, .box_w = 4, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 109, .adv_w = 112, .box_w = 6, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 115, .adv_w = 80, .box_w = 4, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 119, .adv_w = 128, .box_w = 7, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 126, .adv_w = 112, .box_w = 6, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 132, .adv_w = 112, .box_w = 6, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 138, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 143, .adv_w = 112, .box_w = 6, .box_h = 8, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 149, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 154, .adv_w = 80, .box_w = 4, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 158, .adv_w = 112, .box_w = 6, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 164, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 169, .adv_w = 112, .box_w = 6, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 175, .adv_w = 128, .box_w = 7, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 182, .adv_w = 112, .box_w = 6, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 188, .adv_w = 112, .box_w = 6, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 194, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 199, .adv_w = 96, .box_w = 6, .box_h = 1, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 200, .adv_w = 96, .box_w = 5, .box_h = 5, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 204, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 209, .adv_w = 80, .box_w = 4, .box_h = 5, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 212, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 217, .adv_w = 96, .box_w = 5, .box_h = 5, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 221, .adv_w = 64, .box_w = 3, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 224, .adv_w = 96, .box_w = 5, .box_h = 6, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 228, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 233, .adv_w = 48, .box_w = 2, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 235, .adv_w = 64, .box_w = 3, .box_h = 8, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 238, .adv_w = 96, .box_w = 5, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 243, .adv_w = 48, .box_w = 2, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 245, .adv_w = 144, .box_w = 8, .box_h = 5, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 250, .adv_w = 96, .box_w = 5, .box_h = 5, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 254, .adv_w = 96, .box_w = 5, .box_h = 5, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 258, .adv_w = 96, .box_w = 5, .box_h = 6, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 262, .adv_w = 96, .box_w = 5, .box_h = 6, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 266, .adv_w = 80, .box_w = 4, .box_h = 5, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 269, .adv_w = 80, .box_w = 4, .box_h = 5, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 272, .adv_w = 64, .box_w = 3, .box_h = 7, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 275, .adv_w = 96, .box_w = 5, .box_h = 5, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 279, .adv_w = 96, .box_w = 5, .box_h = 5, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 283, .adv_w = 128, .box_w = 7, .box_h = 5, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 288, .adv_w = 96, .box_w = 5, .box_h = 5, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 292, .adv_w = 96, .box_w = 5, .box_h = 6, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 296, .adv_w = 96, .box_w = 5, .box_h = 5, .ofs_x = 0, .ofs_y = 0}
};

/*---------------------
 *  CHARACTER MAPPING
 *--------------------*/

static const uint8_t glyph_id_ofs_list_0[] = {
    0, 1, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 2, 3, 4, 0,
    5, 6, 7, 8, 9, 10, 11, 12,
    13, 14, 15, 0, 0, 16, 0, 17,
    0, 18, 19, 20, 21, 22, 23, 24,
    25, 26, 27, 28, 29, 30, 31, 32,
    33, 34, 35, 36, 37, 38, 39, 40,
    41, 42, 43, 0, 0, 0, 0, 44
};

/*Collect the unicode lists and glyph_id offsets*/
static const lv_font_fmt_txt_cmap_t cmaps[] =
{
    {
        .range_start = 32, .range_length = 64, .glyph_id_start = 1,
        .unicode_list = NULL, .glyph_id_ofs_list = glyph_id_ofs_list_0, .list_length = 64, .type = LV_FONT_FMT_TXT_CMAP_FORMAT0_FULL
    },
    {
        .range_start = 97, .range_length = 26, .glyph_id_start = 46,
        .unicode_list = NULL, .glyph_id_ofs_list = NULL, .list_length = 0, .type = LV_FONT_FMT_TXT_CMAP_FORMAT0_TINY
    }
};



/*--------------------
 *  ALL CUSTOM DATA
 *--------------------*/

/*Store all the custom data of the font*/
static lv_font_fmt_txt_dsc_t font_dsc = {
    .glyph_bitmap = gylph_bitmap,
    .glyph_dsc = glyph_dsc,
    .cmaps = cmaps,
    .kern_dsc = NULL,
    .kern_scale = 0,
    .cmap_num = 2,
    .bpp = 1,
    .kern_classes = 0,
    .bitmap_format = 0
};


/*-----------------
 *  PUBLIC FONT
 *----------------*/

/*Initialize a public general font descriptor*/
lv_font_t g_font_nokia8 = {
    .get_glyph_dsc = lv_font_get_glyph_dsc_fmt_txt,    /*Function pointer to get glyph's data*/
    .get_glyph_bitmap = lv_font_get_bitmap_fmt_txt,    /*Function pointer to get glyph's bitmap*/
    .line_height = 8,          /*The maximum line height required by the font*/
    .base_line = 1,             /*Baseline measured from the bottom of the line*/
#if !(LVGL_VERSION_MAJOR == 6 && LVGL_VERSION_MINOR == 0)
    .subpx = LV_FONT_SUBPX_NONE,
#endif
    .dsc = &font_dsc           /*The custom font data. Will be accessed by `get_glyph_bitmap/dsc` */
};

#endif /*#if NOKIA8*/

