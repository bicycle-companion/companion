#include <nuttx/config.h>
#include <lvgl/lvgl.h>

/*******************************************************************************
 * Size: 14 px
 * Bpp: 1
 * Opts: --bpp 1 --size 14 --font /home/v01d/Downloads/fonts/forkawesome-webfont.ttf --output ../extra_apps/bicycle_companion/fonts/forkawesome14.c --format lvgl --autohint-off -r 0xf240-0xf245 -r 0xf185-0xf186
 ******************************************************************************/

#ifndef FORKAWESOME14
#define FORKAWESOME14 1
#endif

#if FORKAWESOME14

/*-----------------
 *    BITMAPS
 *----------------*/

/*Store the image of the glyphs*/
static LV_ATTRIBUTE_LARGE_CONST const uint8_t gylph_bitmap[] = {
    /* U+F185 "" */
    0x2, 0x1, 0x39, 0xf, 0xf8, 0x60, 0x46, 0x3,
    0xe0, 0xd, 0x0, 0x48, 0x2, 0xc0, 0x1b, 0x1,
    0xcc, 0x18, 0x7f, 0xc2, 0x72, 0x1, 0x0,

    /* U+F186 "" */
    0xc, 0x3, 0xc0, 0x48, 0x4, 0x80, 0x88, 0x8,
    0x80, 0x8c, 0x8, 0x60, 0x43, 0xe4, 0x2, 0x30,
    0xc0, 0xf0,

    /* U+F240 "" */
    0xff, 0xff, 0xa0, 0x0, 0x2b, 0xff, 0xea, 0xff,
    0xfb, 0xbf, 0xfe, 0x6f, 0xff, 0x9b, 0xff, 0xe6,
    0xff, 0xfa, 0x80, 0x0, 0xbf, 0xff, 0xe0,

    /* U+F241 "" */
    0xff, 0xff, 0xa0, 0x0, 0x2b, 0xff, 0xa, 0xff,
    0xc3, 0xbf, 0xf0, 0x6f, 0xfc, 0x1b, 0xff, 0x6,
    0xff, 0xc2, 0x80, 0x0, 0xbf, 0xff, 0xe0,

    /* U+F242 "" */
    0xff, 0xff, 0xa0, 0x0, 0x2b, 0xf8, 0xa, 0xfe,
    0x3, 0xbf, 0x80, 0x6f, 0xe0, 0x1b, 0xf8, 0x6,
    0xfe, 0x2, 0x80, 0x0, 0xbf, 0xff, 0xe0,

    /* U+F243 "" */
    0xff, 0xff, 0xa0, 0x0, 0x2b, 0xc0, 0xa, 0xf0,
    0x3, 0xbc, 0x0, 0x6f, 0x0, 0x1b, 0xc0, 0x6,
    0xf0, 0x2, 0x80, 0x0, 0xbf, 0xff, 0xe0,

    /* U+F244 "" */
    0xff, 0xff, 0xa0, 0x0, 0x28, 0x0, 0xa, 0x0,
    0x3, 0x80, 0x0, 0x60, 0x0, 0x18, 0x0, 0x6,
    0x0, 0x2, 0x80, 0x0, 0xbf, 0xff, 0xe0,

    /* U+F245 "" */
    0x80, 0x60, 0x38, 0x1e, 0xf, 0x87, 0xe3, 0xf9,
    0xfe, 0xff, 0xfe, 0x3f, 0x19, 0xc8, 0xe0, 0x30
};


/*---------------------
 *  GLYPH DESCRIPTION
 *--------------------*/

static const lv_font_fmt_txt_glyph_dsc_t glyph_dsc[] = {
    {.bitmap_index = 0, .adv_w = 0, .box_w = 0, .box_h = 0, .ofs_x = 0, .ofs_y = 0} /* id = 0 reserved */,
    {.bitmap_index = 0, .adv_w = 213, .box_w = 13, .box_h = 14, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 23, .adv_w = 184, .box_w = 12, .box_h = 12, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 41, .adv_w = 288, .box_w = 18, .box_h = 10, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 64, .adv_w = 288, .box_w = 18, .box_h = 10, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 87, .adv_w = 288, .box_w = 18, .box_h = 10, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 110, .adv_w = 288, .box_w = 18, .box_h = 10, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 133, .adv_w = 288, .box_w = 18, .box_h = 10, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 156, .adv_w = 144, .box_w = 9, .box_h = 14, .ofs_x = 0, .ofs_y = -2}
};

/*---------------------
 *  CHARACTER MAPPING
 *--------------------*/

static const uint16_t unicode_list_0[] = {
    0x0, 0x1, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf, 0xc0
};

/*Collect the unicode lists and glyph_id offsets*/
static const lv_font_fmt_txt_cmap_t cmaps[] =
{
    {
        .range_start = 61829, .range_length = 193, .glyph_id_start = 1,
        .unicode_list = unicode_list_0, .glyph_id_ofs_list = NULL, .list_length = 8, .type = LV_FONT_FMT_TXT_CMAP_SPARSE_TINY
    }
};



/*--------------------
 *  ALL CUSTOM DATA
 *--------------------*/

/*Store all the custom data of the font*/
static lv_font_fmt_txt_dsc_t font_dsc = {
    .glyph_bitmap = gylph_bitmap,
    .glyph_dsc = glyph_dsc,
    .cmaps = cmaps,
    .kern_dsc = NULL,
    .kern_scale = 0,
    .cmap_num = 1,
    .bpp = 1,
    .kern_classes = 0,
    .bitmap_format = 0
};


/*-----------------
 *  PUBLIC FONT
 *----------------*/

/*Initialize a public general font descriptor*/
lv_font_t g_font_forkawesome14 = {
    .get_glyph_dsc = lv_font_get_glyph_dsc_fmt_txt,    /*Function pointer to get glyph's data*/
    .get_glyph_bitmap = lv_font_get_bitmap_fmt_txt,    /*Function pointer to get glyph's bitmap*/
    .line_height = 14,          /*The maximum line height required by the font*/
    .base_line = 2,             /*Baseline measured from the bottom of the line*/
#if !(LVGL_VERSION_MAJOR == 6 && LVGL_VERSION_MINOR == 0)
    .subpx = LV_FONT_SUBPX_NONE,
#endif
    .dsc = &font_dsc           /*The custom font data. Will be accessed by `get_glyph_bitmap/dsc` */
};

#endif /*#if FORKAWESOME14*/

