#include <nuttx/config.h>
#include <lvgl/lvgl.h>

/*******************************************************************************
 * Size: 16 px
 * Bpp: 1
 * Opts: --bpp 1 --size 16 --font /home/v01d/Downloads/fonts/forkawesome-webfont.ttf --output ../extra_apps/bicycle_companion/fonts/forkawesome16.c --format lvgl --autohint-off -r 0xf240-0xf245 -r 0xf185-0xf186
 ******************************************************************************/

#ifndef FORKAWESOME16
#define FORKAWESOME16 1
#endif

#if FORKAWESOME16

/*-----------------
 *    BITMAPS
 *----------------*/

/*Store the image of the glyphs*/
static LV_ATTRIBUTE_LARGE_CONST const uint8_t gylph_bitmap[] = {
    /* U+F185 "" */
    0x1, 0x0, 0x3, 0x0, 0x6e, 0xc0, 0xe1, 0x81,
    0x1, 0xc, 0x1, 0xf8, 0x3, 0xb0, 0x2, 0x40,
    0x4, 0xc0, 0xb, 0x80, 0x39, 0x80, 0x41, 0x81,
    0x3, 0xfe, 0x4, 0xe4, 0x0, 0x80,

    /* U+F186 "" */
    0x2, 0x0, 0xf0, 0xd, 0x0, 0xd8, 0x4, 0x80,
    0x44, 0x2, 0x30, 0x10, 0x80, 0x86, 0x6, 0x18,
    0x50, 0x3e, 0xc0, 0x23, 0x86, 0x7, 0xe0,

    /* U+F240 "" */
    0xff, 0xff, 0xe4, 0x0, 0x1, 0x2f, 0xff, 0xed,
    0x7f, 0xff, 0x7b, 0xff, 0xf9, 0xdf, 0xff, 0xce,
    0xff, 0xfe, 0x77, 0xff, 0xf6, 0xbf, 0xff, 0xa4,
    0x0, 0x1, 0x3f, 0xff, 0xf8,

    /* U+F241 "" */
    0xff, 0xff, 0xe4, 0x0, 0x1, 0x2f, 0xff, 0xd,
    0x7f, 0xf8, 0x7b, 0xff, 0xc1, 0xdf, 0xfe, 0xe,
    0xff, 0xf0, 0x77, 0xff, 0x86, 0xbf, 0xfc, 0x24,
    0x0, 0x1, 0x3f, 0xff, 0xf8,

    /* U+F242 "" */
    0xff, 0xff, 0xe4, 0x0, 0x1, 0x2f, 0xf0, 0xd,
    0x7f, 0x80, 0x7b, 0xfc, 0x1, 0xdf, 0xe0, 0xe,
    0xff, 0x0, 0x77, 0xf8, 0x6, 0xbf, 0xc0, 0x24,
    0x0, 0x1, 0x3f, 0xff, 0xf8,

    /* U+F243 "" */
    0xff, 0xff, 0xe4, 0x0, 0x1, 0x2f, 0x80, 0xd,
    0x7c, 0x0, 0x7b, 0xe0, 0x1, 0xdf, 0x0, 0xe,
    0xf8, 0x0, 0x77, 0xc0, 0x6, 0xbe, 0x0, 0x24,
    0x0, 0x1, 0x3f, 0xff, 0xf8,

    /* U+F244 "" */
    0xff, 0xff, 0xe4, 0x0, 0x1, 0x20, 0x0, 0xd,
    0x0, 0x0, 0x78, 0x0, 0x1, 0xc0, 0x0, 0xe,
    0x0, 0x0, 0x70, 0x0, 0x6, 0x80, 0x0, 0x24,
    0x0, 0x1, 0x3f, 0xff, 0xf8,

    /* U+F245 "" */
    0x80, 0x30, 0xe, 0x3, 0xc0, 0xf8, 0x3f, 0xf,
    0xe3, 0xfc, 0xff, 0xbf, 0xff, 0xff, 0xf8, 0xee,
    0x33, 0xc8, 0x70, 0x1c
};


/*---------------------
 *  GLYPH DESCRIPTION
 *--------------------*/

static const lv_font_fmt_txt_glyph_dsc_t glyph_dsc[] = {
    {.bitmap_index = 0, .adv_w = 0, .box_w = 0, .box_h = 0, .ofs_x = 0, .ofs_y = 0} /* id = 0 reserved */,
    {.bitmap_index = 0, .adv_w = 244, .box_w = 15, .box_h = 16, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 30, .adv_w = 210, .box_w = 13, .box_h = 14, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 53, .adv_w = 329, .box_w = 21, .box_h = 11, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 82, .adv_w = 329, .box_w = 21, .box_h = 11, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 111, .adv_w = 329, .box_w = 21, .box_h = 11, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 140, .adv_w = 329, .box_w = 21, .box_h = 11, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 169, .adv_w = 329, .box_w = 21, .box_h = 11, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 198, .adv_w = 165, .box_w = 10, .box_h = 16, .ofs_x = 0, .ofs_y = -2}
};

/*---------------------
 *  CHARACTER MAPPING
 *--------------------*/

static const uint16_t unicode_list_0[] = {
    0x0, 0x1, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf, 0xc0
};

/*Collect the unicode lists and glyph_id offsets*/
static const lv_font_fmt_txt_cmap_t cmaps[] =
{
    {
        .range_start = 61829, .range_length = 193, .glyph_id_start = 1,
        .unicode_list = unicode_list_0, .glyph_id_ofs_list = NULL, .list_length = 8, .type = LV_FONT_FMT_TXT_CMAP_SPARSE_TINY
    }
};



/*--------------------
 *  ALL CUSTOM DATA
 *--------------------*/

/*Store all the custom data of the font*/
static lv_font_fmt_txt_dsc_t font_dsc = {
    .glyph_bitmap = gylph_bitmap,
    .glyph_dsc = glyph_dsc,
    .cmaps = cmaps,
    .kern_dsc = NULL,
    .kern_scale = 0,
    .cmap_num = 1,
    .bpp = 1,
    .kern_classes = 0,
    .bitmap_format = 0
};


/*-----------------
 *  PUBLIC FONT
 *----------------*/

/*Initialize a public general font descriptor*/
lv_font_t g_font_forkawesome16 = {
    .get_glyph_dsc = lv_font_get_glyph_dsc_fmt_txt,    /*Function pointer to get glyph's data*/
    .get_glyph_bitmap = lv_font_get_bitmap_fmt_txt,    /*Function pointer to get glyph's bitmap*/
    .line_height = 16,          /*The maximum line height required by the font*/
    .base_line = 2,             /*Baseline measured from the bottom of the line*/
#if !(LVGL_VERSION_MAJOR == 6 && LVGL_VERSION_MINOR == 0)
    .subpx = LV_FONT_SUBPX_NONE,
#endif
    .dsc = &font_dsc           /*The custom font data. Will be accessed by `get_glyph_bitmap/dsc` */
};

#endif /*#if FORKAWESOME16*/

