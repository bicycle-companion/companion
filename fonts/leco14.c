#include <nuttx/config.h>
#include <lvgl/lvgl.h>

/*******************************************************************************
 * Size: 14 px
 * Bpp: 1
 * Opts: --bpp 1 --size 14 --font /home/v01d/Downloads/fonts/LECO 1976 Regular.ttf --symbols  -_.,:!?°= --output ../extra_apps/bicycle_companion/fonts/leco14.c --format lvgl --autohint-off -r 48-58 -r 65-90 -r 97-122
 ******************************************************************************/

#ifndef LECO14
#define LECO14 1
#endif

#if LECO14

/*-----------------
 *    BITMAPS
 *----------------*/

/*Store the image of the glyphs*/
static LV_ATTRIBUTE_LARGE_CONST const uint8_t gylph_bitmap[] = {
    /* U+20 " " */
    0x0,

    /* U+21 "!" */
    0xff, 0xfc, 0xf0,

    /* U+2C "," */
    0xff,

    /* U+2D "-" */
    0xff,

    /* U+2E "." */
    0xf0,

    /* U+30 "0" */
    0xff, 0xff, 0x1e, 0x3c, 0x78, 0xf1, 0xe3, 0xff,
    0xfc,

    /* U+31 "1" */
    0xee, 0x66, 0x66, 0x66, 0xff,

    /* U+32 "2" */
    0xff, 0xff, 0x18, 0x3f, 0xff, 0xf0, 0x60, 0xff,
    0xfc,

    /* U+33 "3" */
    0xff, 0xf0, 0xc3, 0x7d, 0xf0, 0xc3, 0xff, 0xf0,

    /* U+34 "4" */
    0xc7, 0x8f, 0x1e, 0x3f, 0xff, 0xc1, 0x83, 0x6,
    0xc,

    /* U+35 "5" */
    0xff, 0xff, 0x6, 0xf, 0xff, 0xc1, 0xe3, 0xff,
    0xfc,

    /* U+36 "6" */
    0xff, 0xff, 0x6, 0xf, 0xff, 0xf1, 0xe3, 0xff,
    0xfc,

    /* U+37 "7" */
    0xff, 0xff, 0x18, 0x30, 0x60, 0xc1, 0x83, 0x6,
    0xc,

    /* U+38 "8" */
    0xff, 0xff, 0x1e, 0x3f, 0xff, 0xf1, 0xe3, 0xff,
    0xfc,

    /* U+39 "9" */
    0xff, 0xff, 0x1e, 0x3f, 0xff, 0xc1, 0x83, 0xff,
    0xfc,

    /* U+3A ":" */
    0xf0, 0xf,

    /* U+3D "=" */
    0xff, 0xf0, 0x0, 0xff, 0xf0,

    /* U+3F "?" */
    0xff, 0xff, 0x18, 0x33, 0xe7, 0xcc, 0x0, 0x30,
    0x60,

    /* U+41 "A" */
    0xff, 0xff, 0x1e, 0x3f, 0xff, 0xf1, 0xe3, 0xc7,
    0x8c,

    /* U+42 "B" */
    0xff, 0xff, 0x1e, 0x3f, 0xff, 0xf1, 0xe3, 0xff,
    0xfc,

    /* U+43 "C" */
    0xff, 0xfc, 0x30, 0xc3, 0xc, 0x30, 0xff, 0xf0,

    /* U+44 "D" */
    0xfd, 0xff, 0x3e, 0x3c, 0x78, 0xf1, 0xe7, 0xff,
    0xf8,

    /* U+45 "E" */
    0xff, 0xfc, 0x30, 0xfb, 0xec, 0x30, 0xff, 0xf0,

    /* U+46 "F" */
    0xff, 0xfc, 0x30, 0xfb, 0xec, 0x30, 0xc3, 0x0,

    /* U+47 "G" */
    0xff, 0xff, 0x6, 0xc, 0xf9, 0xf1, 0xe3, 0xff,
    0xfc,

    /* U+48 "H" */
    0xc7, 0x8f, 0x1e, 0x3f, 0xff, 0xf1, 0xe3, 0xc7,
    0x8c,

    /* U+49 "I" */
    0xff, 0xff, 0xf0,

    /* U+4A "J" */
    0xff, 0xf0, 0xc3, 0xc, 0x30, 0xc3, 0xff, 0xf0,

    /* U+4B "K" */
    0xc7, 0x9b, 0x67, 0x8f, 0xff, 0xf1, 0xe3, 0xc7,
    0x8c,

    /* U+4C "L" */
    0xc6, 0x31, 0x8c, 0x63, 0x18, 0xff, 0xc0,

    /* U+4D "M" */
    0xc0, 0xf8, 0x7e, 0x1f, 0x87, 0xf3, 0xfc, 0xfd,
    0xef, 0x7b, 0xdc, 0xf3, 0x30,

    /* U+4E "N" */
    0xc3, 0xe3, 0xe3, 0xf3, 0xf3, 0xdb, 0xdb, 0xcf,
    0xcf, 0xc7,

    /* U+4F "O" */
    0xff, 0xff, 0x1e, 0x3c, 0x78, 0xf1, 0xe3, 0xff,
    0xfc,

    /* U+50 "P" */
    0xff, 0xff, 0x1e, 0x3f, 0xff, 0xf0, 0x60, 0xc1,
    0x80,

    /* U+51 "Q" */
    0xfe, 0xfe, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6,
    0xfe, 0xfe, 0x7, 0x7,

    /* U+52 "R" */
    0xff, 0xff, 0x1e, 0x3f, 0xff, 0xfc, 0x6c, 0xcd,
    0x8c,

    /* U+53 "S" */
    0xff, 0xff, 0x6, 0xf, 0xff, 0xc1, 0x83, 0xff,
    0xfc,

    /* U+54 "T" */
    0xff, 0xf3, 0xc, 0x30, 0xc3, 0xc, 0x30, 0xc0,

    /* U+55 "U" */
    0xc7, 0x8f, 0x1e, 0x3c, 0x78, 0xf1, 0xe3, 0xff,
    0xfc,

    /* U+56 "V" */
    0xc1, 0xa0, 0x98, 0xcc, 0x62, 0x21, 0xb0, 0xd8,
    0x28, 0x1c, 0xe, 0x0,

    /* U+57 "W" */
    0xc6, 0x16, 0x39, 0x99, 0xcc, 0xce, 0x66, 0xf3,
    0x36, 0xf1, 0xb7, 0x87, 0x3c, 0x38, 0xe1, 0xc6,
    0x0,

    /* U+58 "X" */
    0xc1, 0xb1, 0x8d, 0x86, 0xc1, 0xc0, 0xe0, 0xd8,
    0x6c, 0x63, 0x60, 0xc0,

    /* U+59 "Y" */
    0xc7, 0x8f, 0x1e, 0x3f, 0xff, 0xcc, 0x18, 0x30,
    0x60,

    /* U+5A "Z" */
    0xff, 0xfc, 0x18, 0x3f, 0xff, 0xf0, 0x60, 0xff,
    0xfc,

    /* U+5F "_" */
    0xff, 0xf0,

    /* U+61 "a" */
    0xff, 0xff, 0x18, 0x3f, 0xff, 0xf1, 0xe3, 0xff,
    0xfc,

    /* U+62 "b" */
    0xff, 0xff, 0x63, 0x63, 0x7f, 0x7f, 0x63, 0x63,
    0xff, 0xff,

    /* U+63 "c" */
    0xff, 0xff, 0x1e, 0xc, 0x18, 0x30, 0x63, 0xff,
    0xfc,

    /* U+64 "d" */
    0xff, 0xff, 0x63, 0x63, 0x63, 0x63, 0x63, 0x63,
    0xff, 0xff,

    /* U+65 "e" */
    0xff, 0xff, 0x1e, 0x3f, 0xff, 0xf0, 0x63, 0xff,
    0xfc,

    /* U+66 "f" */
    0xff, 0xff, 0x1e, 0xf, 0x9f, 0x30, 0x60, 0xc1,
    0x80,

    /* U+67 "g" */
    0xff, 0xff, 0x1e, 0x3f, 0xff, 0xc1, 0xe3, 0xff,
    0xfc,

    /* U+68 "h" */
    0xc7, 0x8f, 0x1e, 0x3f, 0xff, 0xf1, 0xe3, 0xc7,
    0x8c,

    /* U+69 "i" */
    0xff, 0xff, 0xf0,

    /* U+6A "j" */
    0xff, 0xf0, 0xc3, 0xc, 0x30, 0xf3, 0xff, 0xf0,

    /* U+6B "k" */
    0xc7, 0x8f, 0x1e, 0x3f, 0xff, 0xf3, 0x66, 0xcf,
    0x9c,

    /* U+6C "l" */
    0xc6, 0x31, 0x8c, 0x63, 0x18, 0xff, 0xc0,

    /* U+6D "m" */
    0xff, 0xff, 0xfc, 0xcf, 0x33, 0xcc, 0xf3, 0x3c,
    0xcf, 0x33, 0xcc, 0xf3, 0x30,

    /* U+6E "n" */
    0xff, 0xff, 0x1e, 0x3c, 0x78, 0xf1, 0xe3, 0xc7,
    0x8c,

    /* U+6F "o" */
    0xff, 0xff, 0x1e, 0x3c, 0x78, 0xf1, 0xe3, 0xff,
    0xfc,

    /* U+70 "p" */
    0xff, 0xff, 0x1e, 0x3f, 0xff, 0xf0, 0x60, 0xc1,
    0x80,

    /* U+71 "q" */
    0xff, 0xff, 0x1e, 0x3c, 0x78, 0xf1, 0xe3, 0xff,
    0xfc, 0x18,

    /* U+72 "r" */
    0xff, 0xff, 0x1e, 0x3f, 0xff, 0xf3, 0x66, 0xcf,
    0x9c,

    /* U+73 "s" */
    0xff, 0xff, 0x1e, 0xf, 0xff, 0xc1, 0xe3, 0xff,
    0xfc,

    /* U+74 "t" */
    0xff, 0xf3, 0xc, 0x30, 0xc3, 0xc, 0x30, 0xc0,

    /* U+75 "u" */
    0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6, 0xc6,
    0xff, 0xff,

    /* U+76 "v" */
    0xc7, 0x8f, 0x1e, 0x3c, 0x78, 0xf1, 0xe3, 0xff,
    0xfc,

    /* U+77 "w" */
    0xcc, 0xf3, 0x3c, 0xcf, 0x33, 0xcc, 0xf3, 0x3c,
    0xcf, 0x33, 0xff, 0xff, 0xf0,

    /* U+78 "x" */
    0xe7, 0xe7, 0x66, 0x66, 0x7e, 0x7e, 0x66, 0x66,
    0xe7, 0xe7,

    /* U+79 "y" */
    0xc7, 0x8f, 0x1e, 0x3f, 0xff, 0xc1, 0xe3, 0xff,
    0xfc,

    /* U+7A "z" */
    0xff, 0xff, 0x18, 0x3f, 0xff, 0xf0, 0x63, 0xff,
    0xfc,

    /* U+B0 "°" */
    0xf9, 0x99, 0xf0
};


/*---------------------
 *  GLYPH DESCRIPTION
 *--------------------*/

static const lv_font_fmt_txt_glyph_dsc_t glyph_dsc[] = {
    {.bitmap_index = 0, .adv_w = 0, .box_w = 0, .box_h = 0, .ofs_x = 0, .ofs_y = 0} /* id = 0 reserved */,
    {.bitmap_index = 0, .adv_w = 71, .box_w = 1, .box_h = 1, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1, .adv_w = 63, .box_w = 2, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 4, .adv_w = 63, .box_w = 2, .box_h = 4, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 5, .adv_w = 94, .box_w = 4, .box_h = 2, .ofs_x = 1, .ofs_y = 4},
    {.bitmap_index = 6, .adv_w = 63, .box_w = 2, .box_h = 2, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 7, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 16, .adv_w = 118, .box_w = 4, .box_h = 10, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 21, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 30, .adv_w = 141, .box_w = 6, .box_h = 10, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 38, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 47, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 56, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 65, .adv_w = 133, .box_w = 7, .box_h = 10, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 74, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 83, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 92, .adv_w = 63, .box_w = 2, .box_h = 8, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 94, .adv_w = 125, .box_w = 6, .box_h = 6, .ofs_x = 1, .ofs_y = 2},
    {.bitmap_index = 99, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 108, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 117, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 126, .adv_w = 141, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 134, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 143, .adv_w = 141, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 151, .adv_w = 133, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 159, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 168, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 177, .adv_w = 63, .box_w = 2, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 180, .adv_w = 133, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 188, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 197, .adv_w = 118, .box_w = 5, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 204, .adv_w = 188, .box_w = 10, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 217, .adv_w = 149, .box_w = 8, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 227, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 236, .adv_w = 133, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 245, .adv_w = 149, .box_w = 8, .box_h = 12, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 257, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 266, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 275, .adv_w = 125, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 283, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 292, .adv_w = 141, .box_w = 9, .box_h = 10, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 304, .adv_w = 204, .box_w = 13, .box_h = 10, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 321, .adv_w = 141, .box_w = 9, .box_h = 10, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 333, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 342, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 351, .adv_w = 125, .box_w = 6, .box_h = 2, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 353, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 362, .adv_w = 141, .box_w = 8, .box_h = 10, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 372, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 381, .adv_w = 141, .box_w = 8, .box_h = 10, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 391, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 400, .adv_w = 133, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 409, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 418, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 427, .adv_w = 63, .box_w = 2, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 430, .adv_w = 133, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 438, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 447, .adv_w = 118, .box_w = 5, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 454, .adv_w = 188, .box_w = 10, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 467, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 476, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 485, .adv_w = 133, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 494, .adv_w = 141, .box_w = 7, .box_h = 11, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 504, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 513, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 522, .adv_w = 125, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 530, .adv_w = 149, .box_w = 8, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 540, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 549, .adv_w = 188, .box_w = 10, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 562, .adv_w = 141, .box_w = 8, .box_h = 10, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 572, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 581, .adv_w = 141, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 590, .adv_w = 102, .box_w = 4, .box_h = 5, .ofs_x = 1, .ofs_y = 5}
};

/*---------------------
 *  CHARACTER MAPPING
 *--------------------*/

static const uint8_t glyph_id_ofs_list_0[] = {
    0, 1, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 2, 3, 4, 0,
    5, 6, 7, 8, 9, 10, 11, 12,
    13, 14, 15, 0, 0, 16, 0, 17,
    0, 18, 19, 20, 21, 22, 23, 24,
    25, 26, 27, 28, 29, 30, 31, 32,
    33, 34, 35, 36, 37, 38, 39, 40,
    41, 42, 43, 0, 0, 0, 0, 44
};

/*Collect the unicode lists and glyph_id offsets*/
static const lv_font_fmt_txt_cmap_t cmaps[] =
{
    {
        .range_start = 32, .range_length = 64, .glyph_id_start = 1,
        .unicode_list = NULL, .glyph_id_ofs_list = glyph_id_ofs_list_0, .list_length = 64, .type = LV_FONT_FMT_TXT_CMAP_FORMAT0_FULL
    },
    {
        .range_start = 97, .range_length = 26, .glyph_id_start = 46,
        .unicode_list = NULL, .glyph_id_ofs_list = NULL, .list_length = 0, .type = LV_FONT_FMT_TXT_CMAP_FORMAT0_TINY
    },
    {
        .range_start = 176, .range_length = 1, .glyph_id_start = 72,
        .unicode_list = NULL, .glyph_id_ofs_list = NULL, .list_length = 0, .type = LV_FONT_FMT_TXT_CMAP_FORMAT0_TINY
    }
};

/*-----------------
 *    KERNING
 *----------------*/


/*Map glyph_ids to kern left classes*/
static const uint8_t kern_left_class_mapping[] =
{
    0, 0, 0, 1, 2, 1, 0, 3,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    4, 0, 0, 0, 0, 0, 5, 0,
    0, 0, 4, 0, 0, 0, 6, 0,
    7, 7, 0, 4, 0, 1, 0, 0,
    0, 0, 0, 4, 0, 0, 0, 0,
    0, 5, 0, 0, 0, 4, 0, 0,
    0, 6, 8, 0, 0, 0, 0, 0,
    0
};

/*Map glyph_ids to kern right classes*/
static const uint8_t kern_right_class_mapping[] =
{
    0, 0, 0, 1, 2, 1, 0, 0,
    0, 0, 3, 0, 0, 4, 0, 0,
    0, 0, 5, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 6, 0,
    7, 7, 0, 8, 0, 1, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 6, 0, 0, 0, 0, 0, 0,
    0
};

/*Kern values between classes*/
static const int8_t kern_class_values[] =
{
    0, 0, -24, -31, 0, -24, -24, -16,
    0, 0, 0, -16, 0, -16, -8, 0,
    0, 0, -8, -8, 0, 0, 0, 0,
    -16, 0, 0, 0, 0, 0, 0, 0,
    0, -16, -24, -24, -8, -16, -16, -16,
    -24, -16, 0, 0, 0, 0, 0, 0,
    -24, -8, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, -8, 0, 0
};


/*Collect the kern class' data in one place*/
static const lv_font_fmt_txt_kern_classes_t kern_classes =
{
    .class_pair_values   = kern_class_values,
    .left_class_mapping  = kern_left_class_mapping,
    .right_class_mapping = kern_right_class_mapping,
    .left_class_cnt      = 8,
    .right_class_cnt     = 8,
};

/*--------------------
 *  ALL CUSTOM DATA
 *--------------------*/

/*Store all the custom data of the font*/
static lv_font_fmt_txt_dsc_t font_dsc = {
    .glyph_bitmap = gylph_bitmap,
    .glyph_dsc = glyph_dsc,
    .cmaps = cmaps,
    .kern_dsc = &kern_classes,
    .kern_scale = 16,
    .cmap_num = 3,
    .bpp = 1,
    .kern_classes = 1,
    .bitmap_format = 0
};


/*-----------------
 *  PUBLIC FONT
 *----------------*/

/*Initialize a public general font descriptor*/
lv_font_t g_font_leco14 = {
    .get_glyph_dsc = lv_font_get_glyph_dsc_fmt_txt,    /*Function pointer to get glyph's data*/
    .get_glyph_bitmap = lv_font_get_bitmap_fmt_txt,    /*Function pointer to get glyph's bitmap*/
    .line_height = 12,          /*The maximum line height required by the font*/
    .base_line = 2,             /*Baseline measured from the bottom of the line*/
#if !(LVGL_VERSION_MAJOR == 6 && LVGL_VERSION_MINOR == 0)
    .subpx = LV_FONT_SUBPX_NONE,
#endif
    .dsc = &font_dsc           /*The custom font data. Will be accessed by `get_glyph_bitmap/dsc` */
};

#endif /*#if LECO14*/

