/****************************************************************************
 * /home/v01d/coding/nuttx_dk08/extra_apps/watch/defered_task.h
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

#ifndef __DEFERED_TASK_H
#define __DEFERED_TASK_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <sys/time.h>
#include <lvgl/lvgl.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Public Types
 ****************************************************************************/

namespace watch
{
  class DeferedTask
  {
    public:
      DeferedTask(void);
      ~DeferedTask(void);

      /* Shorthand for is_armed() */

      operator bool();

      /* Setup callback and start timer with specified timeout. If already
       * programmed, will cancel timer, reset callback and restart timer
       * with new timeout.
       */

      void program(lv_async_cb_t cb, void* user_data,
                   const struct itimerspec &_timeout, bool _absolute);

      /* Changes the timeout for the running timer. If callback
       * was not yet set with program(), this will not do anything. If
       * the timer had expired, this will restart it.
       */

      void set_timeout(itimerspec _timeout, bool _absolute);

      /* Returns true when the timer is running */

      bool is_armed(void);

      /* Restart the timer. Note that if absolute flag was set, this
       * may run immediately if the time already passed
       */

      void reset(void);

      /* Cancel timer */

      void cancel(void);

      struct itimerspec timeout =
      {
        .it_value = {},
        .it_interval = {},
      };

      timer_t timer;
      lv_async_cb_t cb = 0;
      void* user_data = nullptr;
      bool absolute = false;
    };
}

/****************************************************************************
 * Public Data
 ****************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Inline Functions
 ****************************************************************************/

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif // __DEFERED_TASK_H
