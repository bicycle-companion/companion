/****************************************************************************
 * /home/v01d/coding/nuttx_dk08/extra_apps/watch/watchdog.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/timers/watchdog.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include "watchdog.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

bool watch::Watchdog::initialize(void)
{
#ifdef CONFIG_WATCHDOG
  /* Open Watchdog Device */

  fd = open(CONFIG_WATCHDOG_DEVPATH, O_RDWR);

  if (fd < 0)
    {
      perror("open watchdog device");
      return false;
    }
#endif
  return true;
}

watch::Watchdog::~Watchdog(void)
{
#ifdef CONFIG_WATCHDOG
  if (fd >= 0)
    {
      close(fd);
    }
#endif
}

void watch::Watchdog::pet(void)
{
  if (fd >= 0)
    {
      ioctl(fd, WDIOC_KEEPALIVE, 0);
    }

  printf("watchdog pet!\n");
}
