/****************************************************************************
 * /home/v01d/coding/nuttx_dk08/extra_apps/watch/backlight.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <nuttx/timers/pwm.h>
#include <stdio.h>
#include "backlight.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

watch::Backlight::~Backlight()
{
  if (fd >= 0)
    {
      close(fd);
    }
}

bool watch::Backlight::initialize(void)
{
  fd = open("/dev/backlight", O_RDWR);

  if (fd < 0)
    {
      perror("open /dev/backlight device");
      return false;
    }

  return true;
}

void watch::Backlight::set(uint16_t brightness)
{
  if (brightness == 0)
    {
      ioctl(fd, PWMIOC_STOP, 0);
    }
  else
    {
      struct pwm_info_s pwm;
      pwm.frequency = 10000;
      pwm.channels[0].channel = 1;
      pwm.channels[0].duty = brightness;

      ioctl(fd, PWMIOC_SETCHARACTERISTICS, (uintptr_t)&pwm);
      ioctl(fd, PWMIOC_START, 0);
    }
}
