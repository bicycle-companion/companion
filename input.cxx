/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/input.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/input/buttons.h>
#include <arch/board/board.h>

#ifdef CONFIG_ARCH_SIM
#include <nuttx/input/touchscreen.h>
#endif

#include "app.h"
#include "input.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

watch::Input* g_input = NULL;

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

static void button_handler(int signo, FAR siginfo_t *info, FAR void *ucontext)
{
  if (info->si_code != SI_TIMER)
    {
      g_input->on_button(info->si_value.sival_int);
    }
}

static bool input_cb(lv_indev_drv_t *indev_drv, lv_indev_data_t *data)
{
  ((watch::Input*)indev_drv->user_data)->sample_input(data);

  return false;
}

static void feedback_cb(lv_indev_drv_t *indev_drv, uint8_t event)
{
  watch::Input* input = (watch::Input*)indev_drv->user_data;

  if (event == LV_EVENT_LONG_PRESSED)
    {

    }
  else if (event == LV_EVENT_SHORT_CLICKED)
    {

    }
  else if (event == LV_EVENT_PRESSED)
    {
      /* we start a timer on a press, to wake us up after the long
       * press interval is completed. if the timer was already running
       * this simply restarts the timer
       */

      struct itimerspec it;
      it.it_value.tv_sec = 1;
      it.it_value.tv_nsec = 0;
      it.it_interval.tv_sec = 0;
      it.it_interval.tv_nsec = 0;

      input->long_press_task.program([](void* arg)
      {
        /* do nothing, receiving the signal is enough */
        printf("long press check\n");
      }, input, it, false);

      /* Turn on the backlight on interaction start */

      watch::g_app->display.set_backlight(true);

      /* Cancel alarm if it is executing */

      watch::g_app->alarm_feedback_task.cancel();
      watch::g_app->motor.set(0);
    }
  else if (event == LV_EVENT_RELEASED)
    {
      input->long_press_task.cancel();

      watch::g_app->display.set_backlight(false, 5000);
    }
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

watch::Input::Input(void)
{
  g_input = this;
}

watch::Input::~Input(void)
{
  if (indev)
  {
    lv_indev_reset(indev, nullptr);
  }

  if (fd >= 0)
  {
    close(fd);
  }
}

bool watch::Input::initialize(void)
{
  /* Initialize input */

  lv_indev_drv_t input_drv;
  lv_indev_drv_init(&input_drv);
  input_drv.read_cb = &input_cb;
  input_drv.feedback_cb = &feedback_cb;
  input_drv.type = LV_INDEV_TYPE_KEYPAD;
  input_drv.user_data = this;
  input_drv.long_press_time = 1000;
  indev = lv_indev_drv_register(&input_drv);

  struct btn_notify_s notify;
  fd = open("/dev/buttons", O_RDONLY | O_NONBLOCK);

  if (fd < 0)
    {
      return false;
    }

  /* register button handler */

  struct sigaction act;
  act.sa_sigaction = button_handler;
  act.sa_mask      = 0;
  act.sa_flags     = SA_SIGINFO;
  sigaction(SIGUSR1, &act, NULL);

  /* request notification for button state changes */

  notify.bn_press   = 1;
  notify.bn_release = 1;
  notify.bn_event.sigev_notify = SIGEV_SIGNAL;
  notify.bn_event.sigev_signo  = SIGUSR1;
  notify.bn_event.sigev_value.sival_ptr = this;

  ioctl(fd, BTNIOC_REGISTER, (unsigned long)&notify);

  /* unblock signal */

  sigset_t set;
  sigemptyset(&set);
  sigaddset(&set, SIGUSR1);
  sigprocmask(SIG_UNBLOCK, &set, NULL);

  return true;
}

bool watch::Input::is_button_pressed(void)
{
  btn_buttonset_t btnset = 0;
  if (fd >= 0 && read(fd, &btnset, sizeof(btnset)) == sizeof(btnset))
    {
      return btnset;
    }
  else
    {
      /* Sampling the button is critical so we want to ensure that we
       * can read it
       */

      PANIC();
    }
}

void watch::Input::on_button(uint8_t buttonset)
{
  button_pressed = buttonset;
}

void watch::Input::sample_input(lv_indev_data_t* data)
{
  static bool prev_button_state = false;
  bool button_changed = (prev_button_state ^ button_pressed);

  data->key = LV_KEY_ENTER;

  if (button_pressed)
    {
      data->state = LV_INDEV_STATE_PR;
    }
  else
    {
      data->state = LV_INDEV_STATE_REL;
    }

  if (button_changed)
    {
      watch::g_app->display.report_activity();
    }

  prev_button_state = button_pressed;
}

