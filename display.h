/****************************************************************************
 * watch/display.h
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

#ifndef __WATCH_DISPLAY_H
#define __WATCH_DISPLAY_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <lvgl/lvgl.h>
#include "defered_task.h"
#include "backlight.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Public Types
 ****************************************************************************/

namespace watch
{
  class Display
  {
    public:
      Display(void);
      ~Display(void);

      bool initialize(void);

      lv_style_t text_style_leco62, text_style_leco26, text_style_nokia8,
        text_style_koleeko14, text_style_leco14, text_style_leco28,
        text_style_forkawesome14, text_style_forkawesome16;

      lv_style_t bg_style, scr_style, btn_style, list_btn_style,
        list_bg_style, roller_bg_style, roller_sel_style;

      uint32_t inactive_time(void);
      void report_activity(void);

      enum DisplayPower { LCD_SLEEP = 0, LCD_SLOW, LCD_FAST };
      void power(DisplayPower state);

      void force_refresh(void);

      void set_backlight(bool enable, uint32_t timeout = 0);

      void customize_theme(void);

      uint32_t framebuffer_size = 0;
      int fd = -1;

      Backlight backlight;
      DeferedTask backlight_task;

      bool backlight_on = false;

    private:
      lv_disp_buf_t disp_buf;
      lv_disp_t* disp = nullptr;

      lv_theme_t theme;
  };
}

/****************************************************************************
 * Public Data
 ****************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Inline Functions
 ****************************************************************************/

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif // __WATCH_DISPLAY_H
