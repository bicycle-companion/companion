/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/settings.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/mtd/configdata.h>
#include <nuttx/mtd/mtd.h>
#include "settings.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

watch::Settings::~Settings(void)
{
  if (fd >= 0)
  {
    close(fd);
  }
}

bool watch::Settings::initialize(void)
{
  fd = open("/dev/config", O_RDONLY);

  if (fd < 0)
    {
      perror("Could not opening configdata device");
      return false;
    }
  else
    {
      return true;
  }
}

bool watch::Settings::get(watch::SettingID id, uint8_t* data, size_t size)
{
  struct config_data_s configdata;

  configdata.id = id;
  configdata.instance = 0;
  configdata.len = size;
  configdata.configdata = data;

#if 1
  int ret = ioctl(fd, CFGDIOC_GETCONFIG, (unsigned long)&configdata);
#else
  int ret = -1;
#endif

  return (ret >= 0);
}

template<class T>
bool watch::Settings::get(watch::SettingID id, T& data)
{
  struct config_data_s configdata;

  configdata.id = id;
  configdata.instance = 0;
  configdata.len = sizeof(T);
  configdata.configdata = (uint8_t*)&data;

  int ret = ioctl(fd, CFGDIOC_GETCONFIG, (unsigned long)&configdata);

  return (ret >= 0);
}

template<class T>
bool watch::Settings::set(watch::SettingID id, T& data)
{
  struct config_data_s configdata;

  configdata.id = id;
  configdata.instance = 0;
  configdata.len = sizeof(T);
  configdata.configdata = (uint8_t*)&data;

  int ret = ioctl(fd, CFGDIOC_SETCONFIG, (unsigned long)&configdata);

  return (ret >= 0);
}

bool watch::Settings::set(watch::SettingID id, size_t size, uint8_t* data)
{
  struct config_data_s configdata;

  configdata.id = id;
  configdata.instance = 0;
  configdata.len = size;
  configdata.configdata = data;

  int ret = ioctl(fd, CFGDIOC_SETCONFIG, (unsigned long)&configdata);

  return (ret >= 0);
}

bool watch::Settings::reset(void)
{
  return ioctl(fd, MTDIOC_BULKERASE, 0);
}

template bool watch::Settings::get(watch::SettingID id, bool& data);
template bool watch::Settings::get(watch::SettingID id, int8_t& data);
template bool watch::Settings::get(watch::SettingID id, int16_t& data);
template bool watch::Settings::get(watch::SettingID id, int32_t& data);
template bool watch::Settings::get(watch::SettingID id, uint8_t& data);
template bool watch::Settings::get(watch::SettingID id, uint16_t& data);
template bool watch::Settings::get(watch::SettingID id, uint32_t& data);

template bool watch::Settings::set(watch::SettingID id, bool& data);
template bool watch::Settings::set(watch::SettingID id, int8_t& data);
template bool watch::Settings::set(watch::SettingID id, int16_t& data);
template bool watch::Settings::set(watch::SettingID id, int32_t& data);
template bool watch::Settings::set(watch::SettingID id, uint8_t& data);
template bool watch::Settings::set(watch::SettingID id, uint16_t& data);
template bool watch::Settings::set(watch::SettingID id, uint32_t& data);
