/****************************************************************************
 * watch/display.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/lcd/lcd_dev.h>
#include <arch/board/board.h>

#include "app.h"
#include "display.h"
#include "fonts/fonts.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

#ifdef CONFIG_ARCH_SIM
uint8_t* rgb32_buf;
#endif

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

#ifdef CONFIG_ARCH_SIM
static inline lv_color32_t rgb32_to_rgb222(lv_color32_t color)
{
  //printf("rgb:%i %i %i\n", color.ch.red, color.ch.green, color.ch.blue);
  color.ch.red = (color.ch.red / 85) * 85;
  color.ch.green = (color.ch.green / 85) * 85;
  color.ch.blue = (color.ch.blue / 85) * 85;
  //printf("rgb:%i %i %i\n", color.ch.red, color.ch.green, color.ch.blue);

  return color;
}
#else
static inline lv_color_t rgb233_to_rgb222(lv_color_t color)
{
  color.full  = (color.ch.red >> 1) << 6 |
                (color.ch.green >> 1) << 4 |
                color.ch.blue << 2;

  return color;
}
#endif

static void flush_cb(lv_disp_drv_t* disp_drv, const lv_area_t * area, lv_color_t * color_p)
{
  struct lcddev_area_s lcd_area;
  lcd_area.col_start = area->x1;
  lcd_area.col_end = area->x2;
  lcd_area.row_start = area->y1;
  lcd_area.row_end = area->y2;
  lcd_area.data = (uint8_t*)color_p;

  watch::Display* display = (watch::Display*)disp_drv->user_data;

  ioctl(display->fd, LCDDEVIO_PUTAREA, (unsigned long)&lcd_area);

  lv_disp_flush_ready(disp_drv);
}

#ifdef ENABLE_MONITOR
static void monitor_cb(lv_disp_drv_t * disp_drv, uint32_t time, uint32_t px)
{
  printf("flush: %i px in %i ms\n", px, time);
}
#endif

static void rounder_cb(lv_disp_drv_t * disp_drv, lv_area_t * area)
{
  /* Round start and end column to four byte boundary */

  area->x1 &= ~0b11;
  area->x2 |= 0b11;
}

#ifndef CONFIG_ARCH_SIM
void setpix_cb(lv_disp_drv_t* disp_drv, uint8_t * buf, lv_coord_t buf_w,
               lv_coord_t x, lv_coord_t y,
               lv_color_t color, lv_opa_t opa)
{
  if (opa != LV_OPA_TRANSP)
    {
      buf[y * buf_w + x] = rgb233_to_rgb222(color).full;
    }
}
#else
void setpix_cb(lv_disp_drv_t* disp_drv, uint8_t * buf, lv_coord_t buf_w,
               lv_coord_t x, lv_coord_t y,
               lv_color_t color, lv_opa_t opa)
{
  color.ch.alpha = opa;
  ((lv_color_t*)buf)[y * buf_w + x] = rgb32_to_rgb222(color);
}
#endif

/****************************************************************************
 * Public Functions
 ****************************************************************************/

watch::Display::Display()
{
  disp_buf.buf1 = nullptr;
}


watch::Display::~Display()
{
  if (disp)
  {
    lv_disp_remove(disp);
  }

  if (disp_buf.buf1)
  {
    free(disp_buf.buf1);
    disp_buf.buf1 = nullptr;
  }
}

bool watch::Display::initialize()
{
  uint32_t pixel_count;

  /* initialize hardware */

  fd = open("/dev/lcd0", O_DIRECT);

  if (fd < 0)
    {
      return false;
    }

  /* Start in slow mode */

  power(DisplayPower::LCD_SLOW);

  /* Get LCD parameters */

  struct fb_videoinfo_s vinfo;
  ioctl(fd, LCDDEVIO_GETVIDEOINFO, (unsigned long)&vinfo);

  struct lcd_planeinfo_s pinfo;
  ioctl(fd, LCDDEVIO_GETPLANEINFO, (unsigned long)&pinfo);

  pixel_count = vinfo.xres * vinfo.yres;
  framebuffer_size = pixel_count * (pinfo.bpp >> 3);

  /* create video memory */

  uint8_t* buffer = (uint8_t*)malloc(framebuffer_size);
  memset(buffer, 0, framebuffer_size);
  lv_disp_buf_init(&disp_buf, buffer, nullptr, pixel_count);

  /* create display driver */

  lv_disp_drv_t disp_drv;                 /*A variable to hold the drivers. Can be local variable*/
  lv_disp_drv_init(&disp_drv);            /*Basic initialization*/
  disp_drv.buffer = &disp_buf;            /*Set an initialized buffer*/
  disp_drv.flush_cb = flush_cb;           /*Set a flush callback to draw to the display*/
  disp_drv.rounder_cb = rounder_cb;
  disp_drv.set_px_cb = setpix_cb;         /* TODO: solve color format conversion more efficiently */
#ifdef ENABLE_MONITOR
  disp_drv.monitor_cb = monitor_cb;
#endif
  disp_drv.user_data = this;

  disp = lv_disp_drv_register(&disp_drv); /*Register the driver and save the created display objects*/

  /* Apply theme */

  customize_theme();

  /* Initialize backlight */

  if (!backlight.initialize())
    {
      return false;
    }

  return true;
}

uint32_t watch::Display::inactive_time(void)
{
  return lv_disp_get_inactive_time(disp);
}

void watch::Display::report_activity(void)
{
  lv_disp_trig_activity(disp);
}

void watch::Display::power(DisplayPower state)
{
  ioctl(fd, LCDDEVIO_SETPOWER, (int)state);
}

void watch::Display::force_refresh(void)
{
  lv_refr_now(disp);
}

void watch::Display::set_backlight(bool enable, uint32_t timeout)
{
  if (enable)
    {
      /* turn on backlight */
#ifdef CONFIG_ARCH_SIM
      printf("backlight enable\n");
#else
      backlight.set(16384);
      power(DisplayPower::LCD_FAST);
#endif
      backlight_on = true;

      /* cancel backlight off timer */

      backlight_task.cancel();
    }
  else
    {
      if (timeout == 0)
        {
          /* turn off immediately */
#ifdef CONFIG_ARCH_SIM
          printf("backlight disable\n");
#else
          backlight.set(0);
          power(DisplayPower::LCD_SLOW);
#endif
          backlight_on = false;

          /* cancel backlight off timer */

          backlight_task.cancel();
        }
      else
        {
          /* turn off after some time */

          if (!backlight_task)
            {
              struct itimerspec t;
              t.it_interval = {};
              t.it_value = { .tv_sec = 5 };

              /* timeout not already running, setup */

              backlight_task.program([](void* arg)
              {
                watch::Display *display = (watch::Display*)arg;

#ifdef CONFIG_ARCH_SIM
                printf("backlight disable\n");
#else
                display->backlight.set(0);
                display->power(DisplayPower::LCD_SLOW);
#endif
                display->backlight_on = false;

              }, this, t, false);
            }
          else
            {
              /* timeout already running, reset timeout */

              backlight_task.reset();
            }
        }
    }
}

void watch::Display::customize_theme(void)
{
  /* create styles for specific fonts */

  lv_style_init(&text_style_leco62);
  lv_style_init(&text_style_leco26);
  lv_style_init(&text_style_leco28);
  lv_style_init(&text_style_leco14);
  lv_style_init(&text_style_nokia8);
  lv_style_init(&text_style_koleeko14);
  lv_style_init(&text_style_forkawesome14);
  lv_style_init(&text_style_forkawesome16);

  lv_style_set_text_font(&text_style_leco62, LV_STATE_DEFAULT, &g_font_leco62);
  lv_style_set_text_font(&text_style_leco26, LV_STATE_DEFAULT, &g_font_leco26);
  lv_style_set_text_font(&text_style_leco28, LV_STATE_DEFAULT, &g_font_leco28);
  lv_style_set_text_font(&text_style_leco14, LV_STATE_DEFAULT, &g_font_leco14);
  lv_style_set_text_font(&text_style_nokia8, LV_STATE_DEFAULT, &g_font_nokia8);
  lv_style_set_text_font(&text_style_koleeko14, LV_STATE_DEFAULT, &g_font_koleeko14);
  lv_style_set_text_font(&text_style_forkawesome14, LV_STATE_DEFAULT, &g_font_forkawesome14);
  lv_style_set_text_font(&text_style_forkawesome16, LV_STATE_DEFAULT, &g_font_forkawesome16);

  /* create styles */

  lv_style_init(&scr_style);
  lv_style_set_bg_color(&scr_style, LV_STATE_DEFAULT, LV_COLOR_BLACK);
  lv_style_set_text_color(&scr_style, LV_STATE_DEFAULT, LV_COLOR_WHITE);
  lv_style_set_value_color(&scr_style, LV_STATE_DEFAULT, LV_COLOR_WHITE);
  lv_style_set_value_font(&scr_style, LV_STATE_DEFAULT, &g_font_nokia8);

  lv_style_init(&bg_style);
  lv_style_set_radius(&bg_style, LV_STATE_DEFAULT, 0);
  lv_style_set_bg_color(&bg_style, LV_STATE_DEFAULT, LV_COLOR_BLACK);
  lv_style_set_border_color(&bg_style, LV_STATE_DEFAULT, LV_COLOR_RED);
  lv_style_set_border_width(&bg_style, LV_STATE_DEFAULT, 0);
  lv_style_set_border_color(&bg_style, LV_STATE_FOCUSED, LV_COLOR_GRAY);
  lv_style_set_value_font(&bg_style, LV_STATE_DEFAULT, &g_font_nokia8);
  lv_style_set_text_color(&bg_style, LV_STATE_DEFAULT, LV_COLOR_WHITE);
  lv_style_set_text_font(&bg_style, LV_STATE_DEFAULT, &g_font_nokia8);
  lv_style_set_text_font(&bg_style, LV_STATE_FOCUSED, &g_font_nokia8);
  lv_style_set_text_font(&bg_style, LV_STATE_PRESSED, &g_font_nokia8);
  lv_style_set_text_font(&bg_style, LV_STATE_CHECKED, &g_font_nokia8);

  lv_style_init(&btn_style);
  lv_style_set_radius(&btn_style, LV_STATE_DEFAULT, 0);
  lv_style_set_border_width(&btn_style, LV_STATE_DEFAULT, 0);
  lv_style_set_border_width(&btn_style, LV_STATE_FOCUSED, 0);
  lv_style_set_outline_width(&btn_style, LV_STATE_FOCUSED, 0);
  lv_style_set_bg_color(&btn_style, LV_STATE_DEFAULT, LV_COLOR_RED);
  lv_style_set_bg_color(&btn_style, LV_STATE_FOCUSED, LV_COLOR_ORANGE);
  lv_style_set_bg_color(&btn_style, LV_STATE_PRESSED, lv_color_lighten(LV_COLOR_ORANGE, LV_OPA_50));
  lv_style_set_text_color(&btn_style, LV_STATE_DEFAULT, LV_COLOR_WHITE);
  lv_style_set_text_color(&btn_style, LV_STATE_FOCUSED, LV_COLOR_WHITE);

  lv_style_init(&list_btn_style);
  lv_style_set_border_width(&list_btn_style, LV_STATE_DEFAULT, 0);
  lv_style_set_border_width(&list_btn_style, LV_STATE_FOCUSED, 0);
  lv_style_set_text_color(&list_btn_style, LV_STATE_DEFAULT, LV_COLOR_WHITE);
  lv_style_set_text_color(&list_btn_style, LV_STATE_FOCUSED, LV_COLOR_WHITE);
  lv_style_set_bg_color(&list_btn_style, LV_STATE_DEFAULT, LV_COLOR_BLACK);
  lv_style_set_bg_color(&list_btn_style, LV_STATE_FOCUSED, LV_COLOR_RED);
  lv_style_set_bg_color(&list_btn_style, LV_STATE_PRESSED, lv_color_lighten(LV_COLOR_RED, LV_OPA_50));
  lv_style_set_outline_opa(&list_btn_style, LV_STATE_DEFAULT, LV_OPA_TRANSP);

  lv_style_init(&roller_bg_style);
  lv_style_set_border_width(&roller_bg_style, LV_STATE_DEFAULT, 1);
  lv_style_set_radius(&roller_bg_style, LV_STATE_DEFAULT, 10);
  lv_style_set_border_color(&roller_bg_style, LV_STATE_DEFAULT, LV_COLOR_GRAY);
  lv_style_set_border_color(&roller_bg_style, LV_STATE_FOCUSED, LV_COLOR_RED);
  lv_style_set_border_color(&roller_bg_style, LV_STATE_HOVERED, LV_COLOR_RED);

  lv_style_init(&roller_sel_style);

  /* create theme */

  lv_theme_t * base_theme = lv_theme_get_act();

  lv_theme_copy(&theme, base_theme);
  theme.user_data = this;

  lv_theme_set_base(&theme, base_theme);
  lv_theme_set_apply_cb(&theme, [](lv_theme_t * th, lv_obj_t * obj, lv_theme_style_t name)
  {
    lv_style_list_t * list;
    watch::Display *display = (watch::Display *)th->user_data;

    switch(name)
    {
     case LV_THEME_SCR:
       list = lv_obj_get_style_list(obj, LV_OBJ_PART_MAIN);
       _lv_style_list_add_style(list, &display->scr_style);
       break;
      case LV_THEME_OBJ:
        list = lv_obj_get_style_list(obj, LV_OBJ_PART_MAIN);
        _lv_style_list_add_style(list, &display->bg_style);
        break;
#if LV_USE_CONT
      case LV_THEME_CONT:
        list = lv_obj_get_style_list(obj, LV_CONT_PART_MAIN);
        _lv_style_list_add_style(list, &display->bg_style);
        break;
#endif
#if LV_USE_BTN
      case LV_THEME_BTN:
        list = lv_obj_get_style_list(obj, LV_BTN_PART_MAIN);
        _lv_style_list_add_style(list, &display->btn_style);
        break;
#endif
#if LV_USE_LIST
      case LV_THEME_LIST:
        list = lv_obj_get_style_list(obj, LV_LIST_PART_BG);
        _lv_style_list_add_style(list, &display->bg_style);
#if 0
        _lv_style_list_add_style(list, &display->list_bg_style);

        list = lv_obj_get_style_list(obj, LV_LIST_PART_SCROLLBAR);
        _lv_style_list_add_style(list, &disp->sb);
#endif
        break;
      case LV_THEME_LIST_BTN:
        list = lv_obj_get_style_list(obj, LV_BTN_PART_MAIN);
        _lv_style_list_add_style(list, &display->list_btn_style);
        break;
#endif
#if LV_USE_ROLLER
      case LV_THEME_ROLLER:
        list = lv_obj_get_style_list(obj, LV_ROLLER_PART_BG);
        _lv_style_list_add_style(list, &display->bg_style);
        _lv_style_list_add_style(list, &display->roller_bg_style);

        list = lv_obj_get_style_list(obj, LV_ROLLER_PART_SELECTED);
        _lv_style_list_add_style(list, &display->roller_sel_style);
        break;
#endif
#if LV_USE_SWITCH
      case LV_THEME_SWITCH:
        list = lv_obj_get_style_list(obj, LV_SWITCH_PART_BG);
        _lv_style_list_add_style(list, &display->bg_style);

        list = lv_obj_get_style_list(obj, LV_SWITCH_PART_INDIC);
        _lv_style_list_add_style(list, &display->bg_style);

        list = lv_obj_get_style_list(obj, LV_SWITCH_PART_KNOB);
        _lv_style_list_add_style(list, &display->btn_style);
        break;
#endif

      default:
        break;
    }
  });

  lv_theme_set_act(&theme);
}
