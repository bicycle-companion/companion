/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/speed_screen.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <math.h>

#include <sys/boardctl.h>

#ifndef CONFIG_ARCH_SIM
#include <arch/board/boardctl.h>
#endif

#include "speed_screen.h"
#include "app.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/


void watch::SpeedScreen::enter(watch::Screen* prev_screen)
{
  g_app->set_alarm_period(1);
}

void watch::SpeedScreen::initialize(void)
{
  scr = lv_obj_create(nullptr, nullptr);

  /* speed */
  lv_obj_t* speed_big_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(speed_big_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_leco62);

  lv_obj_t* speed_small_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(speed_small_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_leco26);
  lv_obj_set_x(speed_small_label, 105);
  lv_obj_set_y(speed_small_label, 46);

  lv_obj_t* speed_unit_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(speed_unit_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);
  lv_obj_set_x(speed_unit_label, 107);
  lv_obj_set_y(speed_unit_label, 33);

  /* temperature */
  lv_obj_t* temp_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(temp_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_leco14);
  lv_obj_set_x(temp_label, 3);
  lv_obj_set_y(temp_label, 3);

  /* time */
  lv_obj_t* time_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(time_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_leco14);
  lv_obj_set_x(time_label, 87);
  lv_obj_set_y(time_label, 3);

  /* orientation */

  lv_obj_t* orientation_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(orientation_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_leco14);

  /* distance */

  lv_obj_t* dist_big_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(dist_big_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_leco28);

  lv_obj_t* dist_small_label = lv_label_create(scr, nullptr);

  lv_obj_add_style(dist_small_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_leco14);

  lv_obj_t* dist_unit_label = lv_label_create(scr, nullptr);

  lv_obj_add_style(dist_unit_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);
  lv_obj_set_pos(dist_unit_label, 113, 76);
  lv_label_set_text(dist_unit_label, "km");

  /* altitude */

  lv_obj_t* alt_big_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(alt_big_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_leco28);

  lv_obj_t* alt_small_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(alt_small_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_leco14);

  lv_obj_t* alt_unit_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(alt_unit_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);
  lv_obj_set_pos(alt_unit_label, 113, 108);
  lv_label_set_text(alt_unit_label, "m");

  /* battery */

  lv_obj_t* battery_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(battery_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_forkawesome14);
  lv_label_set_text(battery_label, LV_SYMBOL_BATTERY_2);
  lv_obj_set_pos(battery_label, 107, 20);

  /* dist icon */

  lv_obj_t* dist_icon_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(dist_icon_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_forkawesome16);
  lv_label_set_text(dist_icon_label, "\uF018");
  lv_obj_set_pos(dist_icon_label, 3, 80);

  /* alt icon */

  lv_obj_t* alt_icon_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(alt_icon_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_forkawesome16);
  lv_label_set_text(alt_icon_label, "\uF176");
  lv_obj_set_pos(alt_icon_label, 7, 109);
}

void watch::SpeedScreen::update(void)
{
  printf("updating speed\n");

  /* compute speed */

  float speed = g_app->odometer.speed();

  lv_obj_t* speed_big_label = lv_obj_get_child_back(scr, nullptr);
  lv_label_set_text_fmt(speed_big_label, "%i", (uint32_t)speed);

  lv_obj_t* speed_small_label = lv_obj_get_child_back(scr, speed_big_label);
  lv_label_set_text_fmt(speed_small_label, "%i", (uint32_t)((speed - (uint32_t)speed) * 10));

  lv_obj_align(speed_big_label, speed_small_label, LV_ALIGN_IN_BOTTOM_LEFT,
               -lv_obj_get_width(speed_big_label), 5);

  lv_obj_t* speed_unit_label = lv_obj_get_child_back(scr, speed_small_label);
  lv_label_set_text(speed_unit_label, "kmh");

  /* update temperature */

  float temperature = g_app->baro.get_temperature();
  lv_obj_t* temp_label = lv_obj_get_child_back(scr, speed_unit_label);
  lv_label_set_text_fmt(temp_label, "%.1fC", temperature);
  lv_obj_align(temp_label, NULL, LV_ALIGN_IN_TOP_LEFT, 3, 2);

  /* update time */

  time_t now = time(nullptr);
  struct tm* now_tm = localtime(&now);

  lv_obj_t* time_label = lv_obj_get_child_back(scr, temp_label);

  char time_str[7];
  strftime(time_str, sizeof(time_str) - 1, "%H:%M", now_tm);
  lv_label_set_text(time_label, time_str);

  lv_obj_align(time_label, NULL, LV_ALIGN_IN_TOP_RIGHT, -3, 2);

  /* update orientation */

  float roll, pitch;
  g_app->acc.enable(true);
  g_app->acc.get_orientation(roll, pitch);
  g_app->acc.enable(false);

  g_app->mag.enable(true);
  float orientation = g_app->mag.get_orientation(roll, pitch) * 180.0f / M_PI_F;
  g_app->mag.enable(false);

  lv_obj_t* orientation_label = lv_obj_get_child_back(scr, time_label);
  lv_label_set_text_fmt(orientation_label, "%i°", (int)orientation);
  lv_obj_align(orientation_label, NULL, LV_ALIGN_IN_TOP_MID, 0, 2);

  /* update distance */

  float distance = g_app->odometer.distance();

  lv_obj_t* dist_big_label = lv_obj_get_child_back(scr, orientation_label);
  lv_label_set_text_fmt(dist_big_label, "%i", (uint32_t)distance);
  lv_obj_align(dist_big_label, NULL, LV_ALIGN_IN_RIGHT_MID, -17, 23);

  lv_obj_t* dist_small_label = lv_obj_get_child_back(scr, dist_big_label);
  lv_label_set_text_fmt(dist_small_label, "%i", (uint32_t)((distance - (uint32_t)distance) * 10));
  lv_obj_align(dist_small_label, NULL, LV_ALIGN_IN_RIGHT_MID, -7, 27);

  lv_obj_t* dist_unit_label = lv_obj_get_child_back(scr, dist_small_label);

  /* update altitude */

  float altitude = g_app->baro.get_altitude();

  lv_obj_t* alt_big_label = lv_obj_get_child_back(scr, dist_unit_label);
  lv_label_set_text_fmt(alt_big_label, "%i", (int)altitude);
  lv_obj_align(alt_big_label, NULL, LV_ALIGN_IN_RIGHT_MID, -17, 54);

  lv_obj_t* alt_small_label = lv_obj_get_child_back(scr, alt_big_label);
  lv_label_set_text_fmt(alt_small_label, "%i", (int)((altitude - (int)altitude) * 10));
  lv_obj_align(alt_small_label, NULL, LV_ALIGN_IN_RIGHT_MID, -7, 58);

  lv_obj_t* alt_unit_label = lv_obj_get_child_back(scr, alt_small_label);

  /* update battery */

  const char* battery_symbol;

  if (g_app->battery.state() == Battery::State::CHARGED)
    {
      battery_symbol = LV_SYMBOL_BATTERY_FULL;
    }
  else if (g_app->battery.state() == Battery::State::NO_BATTERY)
    {
      battery_symbol = LV_SYMBOL_BATTERY_EMPTY;
    }
  else
    {

      float battery_level = g_app->battery.percent(g_app->battery.voltage());

      printf("battery percent: %f\n", battery_level);
      if (battery_level < 25)
        {
          battery_symbol = LV_SYMBOL_BATTERY_EMPTY;
        }
      else if (battery_level < 50)
        {
          battery_symbol = LV_SYMBOL_BATTERY_1;
        }
      else if (battery_level < 75)
        {
          battery_symbol = LV_SYMBOL_BATTERY_2;
        }
      else if (battery_level < 100)
        {
          battery_symbol = LV_SYMBOL_BATTERY_3;
        }
      else
        {
          battery_symbol = LV_SYMBOL_BATTERY_FULL;
        }
    }

  lv_obj_t* battery_label = lv_obj_get_child_back(scr, alt_unit_label);
  lv_label_set_text(battery_label, battery_symbol);
}
