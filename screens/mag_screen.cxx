/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/screens/mag_screen.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <math.h>
#include "app.h"
#include "mag_screen.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define MAG_USE_ARC 0

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

static void format_cb(lv_obj_t* gauge, char* buf, int bufsize, int32_t value)
{
  value = (value + 180) % 360;

  switch (value)
  {
    case 0:   strncpy(buf, "N",  bufsize); break;
    case 45:  strncpy(buf, "NE", bufsize); break;
    case 90:  strncpy(buf, "E",  bufsize); break;
    case 135: strncpy(buf, "SE", bufsize); break;
    case 180: strncpy(buf, "S",  bufsize); break;
    case 225: strncpy(buf, "SW", bufsize); break;
    case 270: strncpy(buf, "W",  bufsize); break;
    case 315: strncpy(buf, "NW", bufsize); break;
    default: strncpy(buf, "?", bufsize); break;
  }
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/


void watch::MagScreen::initialize(void)
{
  scr = lv_obj_create(nullptr, nullptr);

#if MAG_USE_ARC
  lv_obj_t* arc = lv_arc_create(scr, nullptr);
  lv_arc_set_bg_angles(arc, 0, 360);

  lv_obj_set_size(arc, CONFIG_LVGL_HOR_RES_MAX - 3, CONFIG_LVGL_VER_RES_MAX - 3);
  lv_obj_align(arc, nullptr, LV_ALIGN_CENTER, 0, 0);
#else
  lv_obj_t* gauge = lv_gauge_create(scr, nullptr);
  lv_obj_add_style(gauge, LV_GAUGE_PART_MAIN, &g_app->display.text_style_leco14);

  static lv_color_t colors[2] = { LV_COLOR_BLACK, LV_COLOR_BLACK };
  lv_gauge_set_needle_count(gauge, 2, colors);
  lv_gauge_set_scale(gauge, 360, 9, 9);
  lv_gauge_set_range(gauge, 0, 360);
  lv_obj_set_size(gauge, CONFIG_LVGL_HOR_RES_MAX - 2, CONFIG_LVGL_VER_RES_MAX - 2);
  lv_obj_align(gauge, scr, LV_ALIGN_CENTER, 0, 0);
  lv_gauge_set_formatter_cb(gauge, format_cb);
  lv_gauge_set_angle_offset(gauge, 90);

  static lv_style_t label_style;
  lv_style_init(&label_style);
  lv_style_set_bg_opa(&label_style, LV_STATE_DEFAULT, LV_OPA_COVER);
  lv_style_set_bg_color(&label_style, LV_STATE_DEFAULT, LV_COLOR_WHITE);
  lv_style_set_border_width(&label_style, LV_STATE_DEFAULT, 1);
  lv_style_set_pad_top(&label_style, LV_STATE_DEFAULT, 3);
  lv_style_set_pad_bottom(&label_style, LV_STATE_DEFAULT, 3);
  lv_style_set_pad_left(&label_style, LV_STATE_DEFAULT, 3);
  lv_style_set_pad_right(&label_style, LV_STATE_DEFAULT, 3);

  lv_obj_t* label = lv_label_create(scr, nullptr);
  lv_obj_add_style(label, LV_LABEL_PART_MAIN, &label_style);
  lv_obj_add_style(label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);

#endif
}

void watch::MagScreen::update(void)
{
  float roll, pitch;
  g_app->acc.enable(true);
  g_app->acc.get_orientation(roll, pitch);
  g_app->acc.enable(false);

  g_app->mag.enable(true);
  float yaw = g_app->mag.get_orientation(roll, pitch) * 180.0f / M_PI_F;
  g_app->mag.enable(false);

#if MAG_USE_ARC
  lv_obj_t* arc = lv_obj_get_child_back(scr, nullptr);

  int arc_angle = (360 - yaw) - 90;
  lv_arc_set_angles(arc, arc_angle - 3, arc_angle + 3);
#else
  lv_obj_t* gauge = lv_obj_get_child_back(scr, nullptr);

  lv_gauge_set_value(gauge, 0, yaw);
  lv_gauge_set_value(gauge, 1, (int)(yaw + 180) % 360);

  lv_obj_t* label = lv_obj_get_child_back(scr, gauge);
  lv_label_set_text_fmt(label, "%i", (int)yaw);
  lv_obj_align(label, scr, LV_ALIGN_CENTER, 0, 0);
#endif
}

void watch::MagScreen::enter(watch::Screen* prev_screen)
{
  g_app->set_alarm_period(1);
}
