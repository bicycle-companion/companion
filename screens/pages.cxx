/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/pages.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#ifndef CONFIG_ARCH_SIM
#include <arch/board/boardctl.h>
#endif

#include <math.h>
#include "app.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

#if 0

void bc::App::create_sensor_screen(void)
{
  lv_obj_t* scr = lv_obj_create(nullptr, nullptr);

  lv_obj_t* mag_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(mag_label, LV_LABEL_PART_MAIN, &text_style_nokia8);

  lv_obj_t* acc_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(acc_label, LV_LABEL_PART_MAIN, &text_style_nokia8);

  pages[PAGE_SENSORS] = scr;
}

void bc::App::update_sensor_screen(void)
{
  printf("update sensors\n");

  mag.enable(true);
  acc.enable(true);

  int16_t magdata[3];
  mag.get_rawdata(magdata);

  lv_obj_t* mag_label = lv_obj_get_child_back(pages[PAGE_SENSORS], nullptr);
  lv_obj_set_pos(mag_label, 2, 0);
  lv_label_set_text_fmt(mag_label, "mag: %i %i %i", magdata[0], magdata[1], magdata[2]);

  int16_t accdata[3];
  acc.get_rawdata(accdata);
  lv_obj_t* acc_label = lv_obj_get_child_back(pages[PAGE_SENSORS], mag_label);
  lv_obj_set_pos(acc_label, 2, 10);
  lv_label_set_text_fmt(acc_label, "acc: %i %i %i", accdata[0], accdata[1], accdata[2]);

  mag.enable(false);
  acc.enable(false);
}
#endif
