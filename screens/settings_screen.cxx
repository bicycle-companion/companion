/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/settings_screen.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <sys/boardctl.h>
#include <arch/board/board.h>
#include <time.h>
#include <nuttx/version.h>
#include "settings_screen.h"
#include "app.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const char *month_names = "Jan\nFeb\nMar\nApr\nMay\nJun\nJul\n"
                                 "Aug\nSep\nOct\nNov\nDec\n";
static const char *year_names = "20\n21\n22\n23\n24\n25\n26\n27\n28\n29\n"
                                "30\n31\n32\n33\n34\n35\n36\n37\n38";
static const char *day_names = "1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12\n13\n14\n"
                               "15\n16\n17\n18\n19\n20\n21\n22\n23\n24\n25\n26"
                               "\n27\n28\n29\n30\n31";
static const char *hour_names = "00\n01\n02\n03\n04\n05\n06\n07\n08\n09\n10"
                                "\n11\n12\n13\n14\n15\n16\n17\n18\n19\n20"
                                "\n21\n22\n23";
static const char *minute_names = "00\n01\n02\n03\n04\n05\n06\n07\n08\n09\n10"
                                  "\n11\n12\n13\n14\n15\n16\n17\n18\n19\n20"
                                  "\n21\n22\n23\n24\n25\n26\n27\n28\n29\n30"
                                  "\n31\n32\n33\n34\n35\n36\n37\n38\n39\n40"
                                  "\n41\n42\n43\n44\n45\n46\n47\n48\n49\n50"
                                  "\n51\n52\n53\n54\n55\n56\n57\n58\n59";

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

void watch::SettingsScreen::date_screen(void)
{
  lv_obj_t *cont = lv_cont_create(scr, nullptr);
  lv_obj_set_style_local_pad_all(cont, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, 0);

  lv_obj_set_user_data(cont, this);
  lv_obj_set_event_cb(cont, [](lv_obj_t * obj, lv_event_t event)
  {
    if (event == LV_EVENT_APPLY)
      {
        lv_obj_t* control_cont = lv_obj_get_child_back(obj, NULL);
        lv_obj_t* day_obj = lv_obj_get_child_back(control_cont, NULL);
        lv_obj_t* month_obj = lv_obj_get_child_back(control_cont, day_obj);
        lv_obj_t* year_obj = lv_obj_get_child_back(control_cont, month_obj);

        struct tm tp;
        time_t t = time(NULL);
        localtime_r(&t, &tp);

        tp.tm_mday = lv_roller_get_selected(day_obj) + 1;
        tp.tm_mon = lv_roller_get_selected(month_obj);
        tp.tm_year = 120 + lv_roller_get_selected(year_obj);

        struct timeval tv;
        tv.tv_sec = mktime(&tp);
        tv.tv_usec = 0;

        settimeofday(&tv, NULL);
      }
  });

  lv_cont_set_layout(cont, LV_LAYOUT_PRETTY_MID);
  lv_cont_set_fit(cont, LV_FIT_MAX);

  lv_group_t *date_group = lv_group_create();
  lv_indev_set_group(watch::g_app->input.indev, date_group);

  lv_obj_t *control_cont = lv_cont_create(cont, nullptr);
  lv_obj_set_style_local_margin_all(control_cont, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, 0);
  lv_obj_set_style_local_border_width(control_cont, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, 0);

  lv_obj_t *day_obj = lv_roller_create(control_cont, nullptr);
  lv_obj_t *month_obj = lv_roller_create(control_cont, nullptr);
  lv_obj_t *year_obj = lv_roller_create(control_cont, nullptr);

  lv_cont_set_layout(control_cont, LV_LAYOUT_PRETTY_MID);
  lv_cont_set_fit2(control_cont, LV_FIT_MAX, LV_FIT_TIGHT);

  lv_group_add_obj(date_group, day_obj);
  lv_group_add_obj(date_group, month_obj);
  lv_group_add_obj(date_group, year_obj);

  lv_obj_set_user_data(day_obj, this);
  lv_obj_set_user_data(month_obj, this);
  lv_obj_set_user_data(year_obj, this);

  auto roller_cb = [](lv_obj_t * roller_obj, lv_event_t roller_event)
  {
    if (roller_event == LV_EVENT_LONG_PRESSED)
      {
        lv_group_focus_next((lv_group_t*)lv_obj_get_group(roller_obj));
      }
    else if (roller_event == LV_EVENT_SHORT_CLICKED)
    {
      lv_roller_set_selected(roller_obj, (lv_roller_get_selected(roller_obj) + 1) %
                             lv_roller_get_option_cnt(roller_obj), LV_ANIM_OFF);
    }
  };

  lv_obj_set_event_cb(day_obj, roller_cb);
  lv_obj_set_event_cb(month_obj, roller_cb);
  lv_obj_set_event_cb(year_obj, roller_cb);

  lv_roller_set_options(day_obj, day_names, LV_ROLLER_MODE_INIFINITE);
  lv_roller_set_options(month_obj, month_names, LV_ROLLER_MODE_INIFINITE);
  lv_roller_set_options(year_obj, year_names, LV_ROLLER_MODE_INIFINITE);

  lv_roller_set_align(day_obj, LV_LABEL_ALIGN_CENTER);
  lv_roller_set_align(month_obj, LV_LABEL_ALIGN_CENTER);
  lv_roller_set_align(year_obj, LV_LABEL_ALIGN_CENTER);

  struct tm tp;
  time_t t = time(NULL);
  localtime_r(&t, &tp);

  lv_roller_set_selected(day_obj, tp.tm_mday - 1, LV_ANIM_OFF);
  lv_roller_set_selected(month_obj, tp.tm_mon, LV_ANIM_OFF);
  lv_roller_set_selected(year_obj, tp.tm_year - 120, LV_ANIM_OFF);

  create_ok_back(cont, date_group);
}

void watch::SettingsScreen::time_screen(void)
{
  lv_obj_t *cont = lv_cont_create(scr, nullptr);

  lv_obj_set_user_data(cont, this);
  lv_obj_set_event_cb(cont, [](lv_obj_t * obj, lv_event_t event)
  {
    if (event == LV_EVENT_APPLY)
      {
        lv_obj_t* control_cont = lv_obj_get_child_back(obj, NULL);
        lv_obj_t* hour_obj = lv_obj_get_child_back(control_cont, NULL);
        lv_obj_t* minute_obj = lv_obj_get_child_back(control_cont, hour_obj);

        struct tm tp;
        time_t t = time(NULL);
        localtime_r(&t, &tp);

        tp.tm_hour = lv_roller_get_selected(hour_obj);
        tp.tm_min = lv_roller_get_selected(minute_obj);

        struct timeval tv;
        tv.tv_sec = mktime(&tp);
        tv.tv_usec = 0;

        settimeofday(&tv, NULL);
      }
  });

  lv_cont_set_layout(cont, LV_LAYOUT_PRETTY_MID);
  lv_cont_set_fit(cont, LV_FIT_MAX);

  lv_obj_set_style_local_pad_all(cont, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, 0);
  lv_obj_set_style_local_pad_inner(cont, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, 0);

  lv_group_t *time_group = lv_group_create();
  lv_indev_set_group(watch::g_app->input.indev, time_group);

  lv_obj_t *control_cont = lv_cont_create(cont, nullptr);

  lv_cont_set_layout(control_cont, LV_LAYOUT_PRETTY_MID);
  lv_cont_set_fit2(control_cont, LV_FIT_MAX, LV_FIT_TIGHT);

  lv_obj_set_style_local_margin_all(control_cont, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, 0);
  lv_obj_set_style_local_pad_inner(control_cont, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, 0);
  lv_obj_set_style_local_pad_left(control_cont, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, 50);
  lv_obj_set_style_local_pad_right(control_cont, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, 50);
  lv_obj_set_style_local_border_width(control_cont, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, 0);

  lv_obj_t *hour_obj = lv_roller_create(control_cont, nullptr);
  lv_obj_t *minute_obj = lv_roller_create(control_cont, nullptr);

  lv_group_add_obj(time_group, hour_obj);
  lv_group_add_obj(time_group, minute_obj);

  lv_obj_set_user_data(hour_obj, this);
  lv_obj_set_user_data(minute_obj, this);

  auto roller_cb = [](lv_obj_t * roller_obj, lv_event_t roller_event)
  {
    if (roller_event == LV_EVENT_LONG_PRESSED)
      {
        lv_group_focus_next((lv_group_t*)lv_obj_get_group(roller_obj));
      }
    else if (roller_event == LV_EVENT_SHORT_CLICKED)
    {
      lv_roller_set_selected(roller_obj, (lv_roller_get_selected(roller_obj) + 1) %
                             lv_roller_get_option_cnt(roller_obj), LV_ANIM_OFF);
    }
  };

  lv_obj_set_event_cb(hour_obj, roller_cb);
  lv_obj_set_event_cb(minute_obj, roller_cb);

  lv_roller_set_options(hour_obj, hour_names, LV_ROLLER_MODE_INIFINITE);
  lv_roller_set_options(minute_obj, minute_names, LV_ROLLER_MODE_INIFINITE);

  lv_roller_set_align(hour_obj, LV_LABEL_ALIGN_CENTER);
  lv_roller_set_align(minute_obj, LV_LABEL_ALIGN_CENTER);

  struct tm tp;
  time_t t = time(NULL);
  localtime_r(&t, &tp);

  lv_roller_set_selected(hour_obj, tp.tm_hour, LV_ANIM_OFF);
  lv_roller_set_selected(minute_obj, tp.tm_min, LV_ANIM_OFF);

  create_ok_back(cont, time_group);
}

void watch::SettingsScreen::alarm_screen(void)
{
  lv_obj_t *cont = lv_cont_create(scr, nullptr);
  lv_obj_set_style_local_pad_all(cont, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, 0);
  lv_obj_set_style_local_pad_inner(cont, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, 0);

  lv_obj_set_user_data(cont, this);
  lv_obj_set_event_cb(cont, [](lv_obj_t * obj, lv_event_t event)
  {
    if (event == LV_EVENT_APPLY)
      {
        lv_obj_t* control_cont = lv_obj_get_child_back(obj, NULL);
        lv_obj_t* hour_obj = lv_obj_get_child_back(control_cont, NULL);
        lv_obj_t* minute_obj = lv_obj_get_child_back(control_cont, hour_obj);
        lv_obj_t* enable_obj = lv_obj_get_child_back(control_cont, minute_obj);

        if (lv_switch_get_state(enable_obj))
          {
            struct tm tp;
            time_t t = time(NULL);
            localtime_r(&t, &tp);

            tp.tm_hour = lv_roller_get_selected(hour_obj);
            tp.tm_min = lv_roller_get_selected(minute_obj);
            tp.tm_sec = 0;

            struct itimerspec timeout;
            timeout.it_value.tv_sec = mktime(&tp);

            if (timeout.it_value.tv_sec < t)
              {
                /* if the time has passed on this day, start the next one */

                timeout.it_value.tv_sec += 3600 * 24;
              }

            timeout.it_value.tv_nsec = 0;
            timeout.it_interval.tv_sec = 3600 * 24; /* 24hr interval */
            timeout.it_interval.tv_nsec = 0;

            watch::g_app->alarm_task.program([](void* arg)
            {
              struct itimerspec feedback_timeout;
              feedback_timeout.it_interval.tv_sec = 1;
              feedback_timeout.it_interval.tv_nsec = 0;
              feedback_timeout.it_value = feedback_timeout.it_interval;

              watch::g_app->alarm_feedback_task.program([](void* arg2)
              {
                static bool state = true;
                if (state)
                  {
                    watch::g_app->motor.set(49152);
                  }
                else
                  {
                    watch::g_app->motor.set(0);
                  }

                state = !state;
              }, nullptr, feedback_timeout, false);
            }, nullptr, timeout, true);
          }
        else
          {
            watch::g_app->alarm_task.cancel();
          }
      }
  });

  lv_cont_set_layout(cont, LV_LAYOUT_PRETTY_MID);
  lv_cont_set_fit(cont, LV_FIT_MAX);

  lv_group_t *time_group = lv_group_create();
  lv_indev_set_group(watch::g_app->input.indev, time_group);

  lv_obj_t *control_cont = lv_cont_create(cont, nullptr);
  lv_obj_set_style_local_margin_all(control_cont, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, 0);
  lv_obj_set_style_local_pad_inner(control_cont, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, 10);
  lv_obj_set_style_local_border_width(control_cont, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, 0);

  lv_cont_set_layout(control_cont, LV_LAYOUT_ROW_MID);
  lv_cont_set_fit2(control_cont, LV_FIT_TIGHT, LV_FIT_TIGHT);

  lv_obj_t *hour_obj = lv_roller_create(control_cont, nullptr);
  lv_obj_t *minute_obj = lv_roller_create(control_cont, nullptr);
  lv_obj_t *enable_obj = lv_switch_create(control_cont, nullptr);

  lv_obj_set_style_local_margin_all(minute_obj, LV_SWITCH_PART_INDIC, LV_STATE_DEFAULT, 10);
  lv_obj_set_style_local_margin_all(hour_obj, LV_SWITCH_PART_INDIC, LV_STATE_DEFAULT, 10);
  lv_obj_set_style_local_margin_all(enable_obj, LV_SWITCH_PART_INDIC, LV_STATE_DEFAULT, 10);

  lv_group_add_obj(time_group, hour_obj);
  lv_group_add_obj(time_group, minute_obj);
  lv_group_add_obj(time_group, enable_obj);

  lv_obj_set_user_data(hour_obj, this);
  lv_obj_set_user_data(minute_obj, this);
  lv_obj_set_user_data(enable_obj, this);

  auto switch_cb = [](lv_obj_t * switch_obj, lv_event_t switch_event)
  {
    if (switch_event == LV_EVENT_LONG_PRESSED)
      {
        lv_group_focus_next((lv_group_t*)lv_obj_get_group(switch_obj));
      }
    else if (switch_event == LV_EVENT_SHORT_CLICKED)
    {
      lv_switch_toggle(switch_obj, LV_ANIM_OFF);
    }
  };

  lv_obj_set_event_cb(enable_obj, switch_cb);

  auto roller_cb = [](lv_obj_t * roller_obj, lv_event_t roller_event)
  {
    if (roller_event == LV_EVENT_LONG_PRESSED)
      {
        lv_group_focus_next((lv_group_t*)lv_obj_get_group(roller_obj));
      }
    else if (roller_event == LV_EVENT_SHORT_CLICKED)
    {
      lv_roller_set_selected(roller_obj, (lv_roller_get_selected(roller_obj) + 1) %
                             lv_roller_get_option_cnt(roller_obj), LV_ANIM_OFF);
    }
  };

  lv_obj_set_event_cb(hour_obj, roller_cb);
  lv_obj_set_event_cb(minute_obj, roller_cb);

  lv_roller_set_options(hour_obj, hour_names, LV_ROLLER_MODE_INIFINITE);
  lv_roller_set_options(minute_obj, minute_names, LV_ROLLER_MODE_INIFINITE);

  lv_roller_set_align(hour_obj, LV_LABEL_ALIGN_CENTER);
  lv_roller_set_align(minute_obj, LV_LABEL_ALIGN_CENTER);

  if (watch::g_app->alarm_task)
    {
      lv_switch_on(enable_obj, LV_ANIM_OFF);

      struct tm tp;
      time_t t = watch::g_app->alarm_task.timeout.it_value.tv_sec;
      localtime_r(&t, &tp);

      lv_roller_set_selected(hour_obj, tp.tm_hour, LV_ANIM_OFF);
      lv_roller_set_selected(minute_obj, tp.tm_min, LV_ANIM_OFF);
    }

  create_ok_back(cont, time_group);
}

void watch::SettingsScreen::about_screen(void)
{
  lv_group_t *new_group = lv_group_create();
  lv_indev_set_group(watch::g_app->input.indev, new_group);

  lv_obj_t *cont = lv_cont_create(scr, nullptr);
  lv_cont_set_fit(cont, LV_FIT_MAX);
  lv_cont_set_layout(cont, LV_LAYOUT_OFF);

  lv_group_add_obj(new_group, cont);

  lv_obj_set_user_data(cont, this);
  lv_obj_set_event_cb(cont, [](lv_obj_t * obj, lv_event_t event)
  {
    SettingsScreen *settings_screen = (SettingsScreen *)obj->user_data;
    if (event == LV_EVENT_LONG_PRESSED)
      {
        lv_group_del((lv_group_t*)lv_obj_get_group(obj));
        lv_obj_del(obj);
        lv_indev_set_group(g_app->input.indev, settings_screen->group);
      }
  });

  struct nrf52_device_info_s info;
  boardctl(BOARDIOC_INFO, (uintptr_t)&info);

  char variant[5];
  for (int i = 0; i < 4; i++)
    {
      variant[i] = 0xFF & (info.variant >> (i * 8));
    }
  variant[4] = 0;

  const char* package;
  switch(info.package)
    {
      case 0x2000:
        package = "QF";
        break;
      case 0x2001:
        package = "CH";
        break;
      case 0x2002:
        package = "CI";
        break;
      case 0x2005:
        package = "CK";
        break;
      default:
        package = "??";
        break;
    }

  lv_obj_t *version_label = lv_label_create(cont, nullptr);
  lv_label_set_text_fmt(version_label, "FW: %s\n"
                                       "OS: %s (%s)\n"
                                       "MCU: NRF%x %s%s\n"
                                       "SRAM: %iKB FLASH: %iKB",
                        "1.0.0", CONFIG_VERSION_STRING, CONFIG_VERSION_BUILD,
                        info.part, package, variant, info.ram, info.flash);
  lv_obj_set_pos(version_label, 15, 15);
  lv_label_set_align(version_label, LV_LABEL_ALIGN_LEFT);
  lv_obj_add_style(version_label, LV_LABEL_PART_MAIN, &watch::g_app->display.text_style_nokia8);
}

void watch::SettingsScreen::create_ok_back(lv_obj_t *parent,
                                           lv_group_t *_group)
{
  lv_obj_t *ok_obj = lv_btn_create(parent, nullptr);
  lv_obj_set_size(ok_obj, 70, 30);
  lv_obj_t* ok_lbl = lv_label_create(ok_obj, nullptr);
  lv_label_set_text_static(ok_lbl, "OK");
  lv_obj_set_user_data(ok_obj, this);
  lv_group_add_obj(_group, ok_obj);
  lv_obj_set_pos(ok_obj, 0, 0);
  lv_obj_set_style_local_margin_left(ok_obj, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, 10);

  lv_obj_set_event_cb(ok_obj, [](lv_obj_t * obj, lv_event_t event)
  {
    SettingsScreen *settings_screen = (SettingsScreen *)obj->user_data;
    if (event == LV_EVENT_SHORT_CLICKED)
      {
        lv_group_focus_next((lv_group_t*)lv_obj_get_group(obj));
      }
    else if (event == LV_EVENT_LONG_PRESSED)
      {
        lv_obj_t *cont = lv_obj_get_parent(obj);
        lv_event_send(cont, LV_EVENT_APPLY, NULL);

        /* TODO: save settings */
        lv_group_del((lv_group_t*)lv_obj_get_group(obj));
        lv_obj_del(cont);
        lv_indev_set_group(g_app->input.indev, settings_screen->group);
      }
  });

  lv_obj_t *back_obj = lv_btn_create(parent, nullptr);
  lv_obj_t *back_lbl = lv_label_create(back_obj, nullptr);
  lv_obj_set_size(back_obj, 70, 30);
  lv_label_set_text_static(back_lbl, "Back");
  lv_obj_set_user_data(back_obj, this);
  lv_group_add_obj(_group, back_obj);
  lv_obj_set_style_local_margin_right(back_obj, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, 10);

  lv_obj_set_event_cb(back_obj, [](lv_obj_t * obj, lv_event_t event)
  {
    SettingsScreen *settings_screen = (SettingsScreen *)obj->user_data;
    if (event == LV_EVENT_SHORT_CLICKED)
      {
        lv_group_focus_next((lv_group_t*)lv_obj_get_group(obj));
      }
    else if (event == LV_EVENT_LONG_PRESSED)
      {
        lv_group_del((lv_group_t*)lv_obj_get_group(obj));
        lv_obj_del(lv_obj_get_parent(obj));
        lv_indev_set_group(g_app->input.indev, settings_screen->group);
      }
  });

}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

void watch::SettingsScreen::initialize(void)
{
  scr = lv_obj_create(nullptr, nullptr);

  /* initialize styles */

  initialize_styles();

  /* create input group */

  group = lv_group_create();

  /* create list */

  list = lv_list_create(scr, nullptr);
  lv_obj_set_size(list, LV_HOR_RES, LV_VER_RES);
  lv_list_set_scrollbar_mode(list, LV_SCROLLBAR_MODE_AUTO);

  lv_obj_add_style(list, LV_LIST_PART_BG, &page_style);

  /* Set date */

  create_menu_button(list, nullptr, "Set date", group,
                                         [](lv_obj_t * obj, lv_event_t event)
  {
    SettingsScreen *settings_screen = (SettingsScreen *)obj->user_data;
    if (event == LV_EVENT_SHORT_CLICKED)
      {
        lv_group_focus_next(settings_screen->group);
      }
    else if (event == LV_EVENT_LONG_PRESSED)
      {
        settings_screen->date_screen();
      }
  });

  /* Set time */

  create_menu_button(list, nullptr, "Set time",
                                         group, [](lv_obj_t * obj, lv_event_t event)
  {
    SettingsScreen *settings_screen = (SettingsScreen *)obj->user_data;
    if (event == LV_EVENT_SHORT_CLICKED)
      {
        lv_group_focus_next(settings_screen->group);
      }
    else if (event == LV_EVENT_LONG_PRESSED)
      {
        settings_screen->time_screen();
      }
  });

  /* Set alarm */

  create_menu_button(list, nullptr, "Set alarm",
                                         group, [](lv_obj_t * obj, lv_event_t event)
  {
    SettingsScreen *settings_screen = (SettingsScreen *)obj->user_data;
    if (event == LV_EVENT_SHORT_CLICKED)
      {
        lv_group_focus_next(settings_screen->group);
      }
    else if (event == LV_EVENT_LONG_PRESSED)
      {
        settings_screen->alarm_screen();
      }
  });

  /* About */

  create_menu_button(list, nullptr, "About",
                                            group, [](lv_obj_t * obj, lv_event_t event)
  {
    SettingsScreen *settings_screen = (SettingsScreen *)obj->user_data;
    if (event == LV_EVENT_SHORT_CLICKED)
      {
        lv_group_focus_next(settings_screen->group);
      }
    else if (event == LV_EVENT_LONG_PRESSED)
      {
        settings_screen->about_screen();
      }
  });

  /* Reboot */

  create_menu_button(list, nullptr, "Reboot",
                                            group, [](lv_obj_t * obj, lv_event_t event)
  {
    SettingsScreen *settings_screen = (SettingsScreen *)obj->user_data;
    if (event == LV_EVENT_SHORT_CLICKED)
      {
        lv_group_focus_next(settings_screen->group);
      }
    else if (event == LV_EVENT_LONG_PRESSED)
      {
#ifndef CONFIG_ARCH_SIM
        boardctl(BOARDIOC_RESET, BOARD_RESET_DFU);
#endif
      }
  });

  create_menu_button(list, nullptr, "Back",
                                     group, [](lv_obj_t * obj, lv_event_t event)
  {
    SettingsScreen *settings_screen = (SettingsScreen *)obj->user_data;
    if (event == LV_EVENT_SHORT_CLICKED)
      {    
        lv_group_focus_next(settings_screen->group); 
      }
    else if (event == LV_EVENT_LONG_PRESSED)
    {      
      watch::g_app->set_prev_screen();
    }
  });
}

void watch::SettingsScreen::enter(watch::Screen *prev_screen)
{
  lv_indev_set_group(g_app->input.indev, group);
}

lv_obj_t* watch::SettingsScreen::create_menu_button(lv_obj_t* parent, const char* symbol,
                                                    const char* label, lv_group_t* _group,
                                                    lv_event_cb_t cb)
{
  lv_obj_t* btn = lv_list_add_btn(parent, symbol, label);

  lv_group_add_obj(_group, btn);
  lv_obj_set_user_data(btn, this);
  lv_obj_set_event_cb(btn, cb);

  return btn;
}

void watch::SettingsScreen::initialize_styles(void)
{
  /* checkbox style */

  lv_style_init(&checkbox_label_style);
  lv_style_set_pad_left(&checkbox_label_style, LV_STATE_DEFAULT, 2);
  lv_style_set_pad_top(&checkbox_label_style, LV_STATE_DEFAULT, 0);
  lv_style_set_pad_bottom(&checkbox_label_style, LV_STATE_DEFAULT, 0);

  lv_style_init(&checkbox_focused_style);
  lv_style_set_bg_opa(&checkbox_label_style, LV_STATE_FOCUSED, LV_OPA_COVER);
  lv_style_set_bg_color(&checkbox_label_style, LV_STATE_FOCUSED, LV_COLOR_BLACK);
  lv_style_set_text_color(&checkbox_label_style, LV_STATE_FOCUSED, LV_COLOR_WHITE);

  lv_style_init(&checkbox_style);
  lv_style_set_radius(&checkbox_style, LV_STATE_DEFAULT, 0);
  lv_style_set_pad_left(&checkbox_style, LV_STATE_DEFAULT, 0);
  lv_style_set_pad_right(&checkbox_style, LV_STATE_DEFAULT, 0);
  lv_style_set_pad_top(&checkbox_style, LV_STATE_DEFAULT, 0);
  lv_style_set_pad_bottom(&checkbox_style, LV_STATE_DEFAULT, 0);
}

lv_obj_t* watch::SettingsScreen::create_checkbox(lv_obj_t* parent, const char* label)
{
  lv_obj_t* cb = lv_checkbox_create(parent, nullptr);

  lv_obj_add_style(cb, LV_CHECKBOX_PART_BG, &g_app->display.text_style_nokia8);
  lv_obj_add_style(cb, LV_CHECKBOX_PART_BG, &checkbox_label_style);
  lv_obj_add_style(cb, LV_CHECKBOX_PART_BG, &checkbox_focused_style);
  lv_obj_add_style(cb, LV_CHECKBOX_PART_BULLET, &checkbox_style);
  lv_checkbox_set_text(cb, label);

  lv_btn_set_fit2(cb, LV_FIT_PARENT, LV_FIT_NONE);
  lv_obj_set_height(cb, 12);

  return cb;
}
