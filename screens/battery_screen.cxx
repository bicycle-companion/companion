/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/screens/battery_screen.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include "app.h"
#include "battery_screen.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

void watch::BatteryScreen::enter(watch::Screen* prev_screen)
{
  watch::g_app->set_alarm_period(1);
}

void watch::BatteryScreen::initialize(void)
{
  scr = lv_obj_create(nullptr, nullptr);

  lv_obj_t* voltage_label = lv_label_create(scr, nullptr);
  lv_obj_set_pos(voltage_label, 5, 5);
  lv_obj_add_style(voltage_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);

  lv_obj_t* percent_label = lv_label_create(scr, nullptr);
  lv_obj_set_pos(percent_label, 5, 15);
  lv_obj_add_style(percent_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);

  lv_obj_t* status_label = lv_label_create(scr, nullptr);
  lv_obj_set_pos(status_label, 5, 25);
  lv_obj_add_style(status_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);

  lv_obj_t* time_label = lv_label_create(scr, nullptr);
  lv_obj_set_pos(time_label, 5, 35);
  lv_obj_add_style(time_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);
}

void watch::BatteryScreen::update()
{
  float v = g_app->battery.voltage();
  lv_obj_t* voltage_label = lv_obj_get_child_back(scr, nullptr);
  lv_label_set_text_fmt(voltage_label, "voltage: %.2f", v);

  lv_obj_t* percent_label = lv_obj_get_child_back(scr, voltage_label);
  lv_label_set_text_fmt(percent_label, "level: %i%%", (int)g_app->battery.percent(v));

  lv_obj_t* status_label = lv_obj_get_child_back(scr, percent_label);

  switch(g_app->battery.state())
  {
    case Battery::State::CHARGED: lv_label_set_text(status_label, "status: charged"); break;
    case Battery::State::UNKNOWN: lv_label_set_text(status_label, "status: unknown"); break;
    case Battery::State::CHARGING: lv_label_set_text(status_label, "status: charging"); break;
    case Battery::State::NO_BATTERY: lv_label_set_text(status_label, "status: no battery"); break;
    case Battery::State::DISCHARGING: lv_label_set_text(status_label, "status: discharging"); break;
    case Battery::State::CRITICALLY_LOW: lv_label_set_text(status_label, "status: critically low"); break;
    case Battery::State::TEMPERATURE_TIMER_FAULT: lv_label_set_text(status_label, "status: temperature/timer fault"); break;
  }

  lv_obj_t* time_label = lv_obj_get_child_back(scr, status_label);
  lv_label_set_text(time_label, "time remaining: ?");
}

