/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/settings_screen.h
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

#ifndef __SETTINGS_SCREEN_H
#define __SETTINGS_SCREEN_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include "screen.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Public Types
 ****************************************************************************/

namespace watch
{
  class SettingsScreen : public Screen
  {
    public:
      SettingsScreen(void) = default;

      void initialize() override;
      void enter(Screen *prev_screen) override;

    private:
      void initialize_styles(void);

      void create_pages(void);
      void create_main_page(void);

      lv_obj_t* create_menu_button(lv_obj_t* parent, const char *symbol, const char* label,
                              lv_group_t* group, lv_event_cb_t cb);
      lv_obj_t* create_checkbox(lv_obj_t* parent, const char* label);


      lv_style_t scrollable_style, page_style;
      lv_style_t checkbox_label_style, checkbox_focused_style,
                 checkbox_style;
      lv_style_t button_style, button_focused_style;

      lv_obj_t *list = nullptr;

      lv_group_t* group = nullptr;

      void date_screen(void);
      void time_screen(void);
      void alarm_screen(void);
      void about_screen(void);
      void create_ok_back(lv_obj_t *parent, lv_group_t *_group);
    };
}

/****************************************************************************
 * Public Data
 ****************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Inline Functions
 ****************************************************************************/

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif // __SETTINGS_SCREEN_H
