/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/screens/temp_screen.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include "app.h"
#include "temp_screen.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

void watch::TempScreen::enter(watch::Screen* prev_screen)
{
  g_app->set_alarm_period(1);
}

void watch::TempScreen::initialize(void)
{
  scr = lv_obj_create(nullptr, nullptr);

  lv_obj_t* temp_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(temp_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);
  lv_obj_set_pos(temp_label, 2, 2);

  lv_obj_t* min_temp_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(min_temp_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);
  lv_obj_set_pos(min_temp_label, 2, 12);

  lv_obj_t* max_temp_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(max_temp_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);
  lv_obj_set_pos(max_temp_label, 2, 22);

  lv_obj_t* plot = lv_line_create(scr, nullptr);
  lv_obj_set_pos(plot, 0, 64);
  lv_line_set_y_invert(plot, true);

  static lv_style_t style_line;
  lv_style_init(&style_line);
  lv_style_set_line_width(&style_line, LV_STATE_DEFAULT, 1);
  lv_style_set_line_color(&style_line, LV_STATE_DEFAULT, LV_COLOR_BLACK);
  lv_style_set_line_rounded(&style_line, LV_STATE_DEFAULT, false);
  lv_obj_add_style(plot, LV_LINE_PART_MAIN, &style_line);     /*Set the points*/
}

void watch::TempScreen::update(void)
{
  lv_obj_t* temp_label = lv_obj_get_child_back(scr, nullptr);
  lv_label_set_text_fmt(temp_label, "temp: %.1f", g_app->baro.get_temperature());

  watch::Baro::statistics_t baro_stats = g_app->baro.get_statistics();

  lv_obj_t* min_temp_label = lv_obj_get_child_back(scr, temp_label);
  lv_label_set_text_fmt(min_temp_label, "min: %.1f", baro_stats.min_temp);

  lv_obj_t* max_temp_label = lv_obj_get_child_back(scr, min_temp_label);
  lv_label_set_text_fmt(max_temp_label, "max: %.1f", baro_stats.max_temp);

  lv_obj_t* plot = lv_obj_get_child_back(scr, max_temp_label);

  int first_valid_point = -1, valid_points = 0;
  for (int i = 0; i < CONFIG_LVGL_HOR_RES_MAX; i++)
  {
    float t = g_app->baro.temp_history[i];
    if (first_valid_point == -1 && t != -99.0f)
    {
      first_valid_point = i;
    }

    if (t != -99.0f)
    {
      plot_points[i - first_valid_point].x = i;
      plot_points[i - first_valid_point].y = (int)(g_app->baro.temp_history[i]);
      valid_points++;
    }
  }
  printf("first: %i n: %i\n", first_valid_point, valid_points);

  lv_line_set_points(plot, plot_points, valid_points);
}

