/****************************************************************************
 * /home/v01d/coding/bicycle-companion/apps/external/bicycle_companion/splash_screen.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include "app.h"
#include "splash_screen.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

static void end_splash(lv_task_t* t)
{
  watch::g_app->set_screen(watch::App::SCREEN_CLOCK);
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

void watch::SplashScreen::initialize(void)
{
  scr = lv_obj_create(nullptr, nullptr);

  lv_obj_t* label1 = lv_label_create(scr, nullptr);
  lv_label_set_text(label1, "Bicycle");
  lv_obj_add_style(label1, LV_LABEL_PART_MAIN, &g_app->display.text_style_leco14);
  lv_obj_set_y(label1, 20);

  lv_obj_t* label2 = lv_label_create(scr, nullptr);
  lv_label_set_text(label2, "Companion");
  lv_obj_add_style(label2, LV_LABEL_PART_MAIN, &g_app->display.text_style_leco14);
  lv_obj_set_y(label2, 40);

  static lv_anim_t anim1, anim2;
  lv_anim_init(&anim1);
  lv_anim_set_var(&anim1, label1);
  lv_anim_set_exec_cb(&anim1, (lv_anim_exec_xcb_t)lv_obj_set_x);
  lv_anim_set_time(&anim1, 1000);
  lv_anim_set_values(&anim1, 128, 20);
  lv_anim_set_repeat_count(&anim1, 0);

  lv_anim_init(&anim2);
  lv_anim_set_var(&anim2, label2);
  lv_anim_set_exec_cb(&anim2, (lv_anim_exec_xcb_t)lv_obj_set_x);
  lv_anim_set_time(&anim2, 1000);
  lv_anim_set_values(&anim2, -80, 40);
  lv_anim_set_repeat_count(&anim2, 0);

  static lv_anim_path_t anim_path1, anim_path2;
  lv_anim_path_init(&anim_path1);
  lv_anim_path_set_cb(&anim_path1, lv_anim_path_ease_out);
  lv_anim_set_path(&anim1, &anim_path1);
  lv_anim_path_set_cb(&anim_path2, lv_anim_path_ease_out);
  lv_anim_set_path(&anim2, &anim_path2);

  lv_anim_start(&anim1);
  lv_anim_start(&anim2);

  lv_task_t* task = lv_task_create(end_splash, 2000, LV_TASK_PRIO_LOW, nullptr);
  lv_task_set_repeat_count(task, 0);
}

void watch::SplashScreen::enter(watch::Screen* prev_screen)
{

}

void watch::SplashScreen::update(void)
{

}

void watch::SplashScreen::exit(watch::Screen* new_screen)
{

}
