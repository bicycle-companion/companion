/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/screens/data_screen.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#ifndef CONFIG_ARCH_SIM
#include <arch/board/boardctl.h>
#endif

#include "data_screen.h"
#include "app.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/


watch::DataScreen::~DataScreen(void)
{

}

void watch::DataScreen::initialize(void)
{
  scr = lv_obj_create(nullptr, nullptr);

  lv_obj_t* mag_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(mag_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);

  lv_obj_t* acc_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(acc_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);

  lv_obj_t* frame_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(frame_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);

  lv_obj_t* standbymode_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(standbymode_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);

  lv_obj_t* pulsecount_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(pulsecount_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);
}

void watch::DataScreen::enter(watch::Screen* prev_screen)
{
  //bc::g_app->set_alarm_period(1);
}

void watch::DataScreen::update(void)
{
  static int i = 0;

  printf("update sensors\n");

  g_app->mag.enable(true);
  int16_t magdata[3];
  g_app->mag.get_rawdata(magdata);
  lv_obj_t* mag_label = lv_obj_get_child_back(scr, nullptr);
  lv_obj_set_pos(mag_label, 2, 0);
  lv_label_set_text_fmt(mag_label, "mag: %i %i %i", magdata[0], magdata[1], magdata[2]);
  g_app->mag.enable(false);

  g_app->acc.enable(true);
  int16_t accdata[3];
  g_app->acc.get_rawdata(accdata);
  lv_obj_t* acc_label = lv_obj_get_child_back(scr, mag_label);
  lv_obj_set_pos(acc_label, 2, 10);
  lv_label_set_text_fmt(acc_label, "acc: %i %i %i", accdata[0], accdata[1], accdata[2]);
  g_app->acc.enable(false);

  lv_obj_t* frame_label = lv_obj_get_child_back(scr, acc_label);
  lv_obj_set_pos(frame_label, 2, 20);
  lv_label_set_text_fmt(frame_label, "frame: %i", i);
  i++;

  lv_obj_t* standbymode_label = lv_obj_get_child_back(scr, frame_label);
  lv_obj_set_pos(standbymode_label, 2, 30);
  uint8_t standbymode;
#ifndef CONFIG_ARCH_SIM
  boardctl(BOARDIOC_GETSTANDBYMODE, (uintptr_t)&standbymode);
#else
  standbymode = 99;
#endif
  lv_label_set_text_fmt(standbymode_label, "standbymode: %i", standbymode);

  if (standbymode == 1)
  {
    // TODO: should only do this once
    watch::g_app->set_alarm_period(1);
  }

  lv_obj_t* pulsecount_label = lv_obj_get_child_back(scr, standbymode_label);
  lv_obj_set_pos(pulsecount_label, 2, 40);
  uint32_t pulsecount;
#ifndef CONFIG_ARCH_SIM
  boardctl(BOARDIOC_PULSECOUNTER_GET, (uintptr_t)&pulsecount);
#else
  pulsecount = 0;
#endif
  lv_label_set_text_fmt(pulsecount_label, "pulsecount: %i", pulsecount);
}
