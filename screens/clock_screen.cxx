/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/clock_screen.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include "clock_screen.h"
#include "app.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

void watch::ClockScreen::enter(watch::Screen* prev_screen)
{
  lv_indev_set_group(watch::g_app->input.indev, group);

  /* Setup a one minute refresh, starting at the top of the next minute */

  struct timespec t;
  clock_gettime(CLOCK_REALTIME, &t);
  t.tv_nsec = 0;
  t.tv_sec = t.tv_sec + (60 - (t.tv_sec % 60));
  watch::g_app->set_refresh_time(60, t, true);
}

void watch::ClockScreen::initialize(void)
{
  scr = lv_obj_create(nullptr, nullptr);
  scr->user_data = this;

  lv_obj_set_event_cb(scr, [](lv_obj_t * obj, lv_event_t event)
  {
    if (event == LV_EVENT_LONG_PRESSED)
      {
        watch::g_app->set_next_screen();
      }
  });

  group = lv_group_create();
  lv_group_add_obj(group, scr);

  /* time/date */

  lv_obj_t* hour_label = lv_label_create(scr, nullptr);
  lv_obj_set_y(hour_label, 20);
  lv_obj_add_style(hour_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_leco62);
  lv_obj_set_style_local_text_color(hour_label, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_BLUE);

  lv_obj_t* minute_label = lv_label_create(scr, nullptr);
  lv_obj_set_y(minute_label, 40);
  lv_obj_add_style(minute_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_leco26);
  lv_obj_set_style_local_text_color(minute_label, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_RED);

  lv_obj_t* date_label = lv_label_create(scr, nullptr);
  lv_obj_set_y(date_label, 40);
  lv_obj_add_style(date_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);

  /* battery */

  lv_obj_t* battery_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(battery_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_forkawesome14);
  //lv_label_set_text(battery_label, LV_SYMBOL_BATTERY_2);
  lv_obj_set_pos(battery_label, 155, 5);

  lv_obj_t* battery_percent_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(battery_percent_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);
  lv_label_set_text(battery_percent_label, LV_SYMBOL_BATTERY_2);
  lv_label_set_align(battery_percent_label, LV_LABEL_ALIGN_RIGHT);
  lv_point_t letter_pos;
  lv_label_get_letter_pos(battery_percent_label, 0, &letter_pos);
  lv_obj_align(battery_percent_label, battery_label,
               LV_ALIGN_OUT_BOTTOM_LEFT, -25, -10);

  /* alarm */

  lv_obj_t* alarm_label = lv_label_create(scr, nullptr);
  lv_obj_set_pos(alarm_label, 5, 5);
  lv_obj_add_style(alarm_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);
  lv_obj_set_style_local_text_color(alarm_label, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_YELLOW);
}

#include <arch/board/board.h>

void watch::ClockScreen::update()
{
  time_t now = time(nullptr);
  struct tm* now_tm = localtime(&now);

  //printf("seconds: %i\n", now_tm->tm_sec);

  /* update time */

  lv_obj_t* hour_label = lv_obj_get_child_back(scr, nullptr);
  lv_obj_t* minute_label = lv_obj_get_child_back(scr, hour_label);
  char time_str[7];

  strftime(time_str, sizeof(time_str) - 1, "%H", now_tm);
  lv_label_set_text(hour_label, time_str);
  lv_obj_align(hour_label, NULL, LV_ALIGN_CENTER, 0, -20);

  strftime(time_str, sizeof(time_str) - 1, "%M", now_tm);
  lv_label_set_text(minute_label, time_str);
  lv_obj_align(minute_label, NULL, LV_ALIGN_CENTER, 0, 15);

  /* update date */

  lv_obj_t* date_label = lv_obj_get_child_back(scr, minute_label);
  char date_str[32];
  strftime(date_str, sizeof(date_str) - 1, "%A\n%b %e %Y", localtime(&now));
  lv_label_set_text(date_label, date_str);
  lv_label_set_align(date_label, LV_LABEL_ALIGN_CENTER);
  lv_obj_align(date_label, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, -5);

  /* update battery */

  const char* battery_symbol;
  float battery_level;
  watch::Battery::State battery_state = g_app->battery.state();

  if (battery_state == Battery::State::CHARGED)
    {
      battery_symbol = LV_SYMBOL_BATTERY_FULL;
      battery_level = 100;
    }
  else if (battery_state == Battery::State::CHARGING)
    {
      battery_symbol = LV_SYMBOL_CHARGE;
      battery_level = 0;
    }
  else
    {
      battery_level = g_app->battery.percent(g_app->battery.voltage());

      printf("battery percent: %f\n", battery_level);
      if (battery_level < 25)
        {
          battery_symbol = LV_SYMBOL_BATTERY_EMPTY;
        }
      else if (battery_level < 50)
        {
          battery_symbol = LV_SYMBOL_BATTERY_1;
        }
      else if (battery_level < 75)
        {
          battery_symbol = LV_SYMBOL_BATTERY_2;
        }
      else if (battery_level < 100)
        {
          battery_symbol = LV_SYMBOL_BATTERY_3;
        }
      else
        {
          battery_symbol = LV_SYMBOL_BATTERY_FULL;
        }
    }

  lv_obj_t* battery_label = lv_obj_get_child_back(scr, date_label);
  //lv_label_set_text(battery_label, battery_symbol);

  bool charged;
  bool powered;
  boardctl(BOARDIOC_CHARGED, (uintptr_t)&charged);
  boardctl(BOARDIOC_CHARGING, (uintptr_t)&powered);

  lv_obj_t* battery_percent_label = lv_obj_get_child_back(scr, battery_label);
  //lv_label_set_text_fmt(battery_percent_label, "% 3i%", battery_level);
  lv_label_set_text_fmt(battery_percent_label, "%i%i %.3f", charged, powered, g_app->battery.voltage());

  lv_obj_t* alarm_label = lv_obj_get_child_back(scr, battery_percent_label);
  if (watch::g_app->alarm_task)
    {
      time_t t = watch::g_app->alarm_task.timeout.it_value.tv_sec;
      struct tm lt;
      localtime_r(&t, &lt);
      lv_label_set_text_fmt(alarm_label, "%02i:%02i", lt.tm_hour, lt.tm_min);
    }
  else
    {
      lv_label_set_text(alarm_label, "");
    }
}
