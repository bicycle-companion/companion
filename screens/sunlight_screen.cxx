/****************************************************************************
 * /home/v01d/coding/bicycle-companion/extra_apps/bicycle_companion/screens/sunlight_screen.cxx
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.  The
 * ASF licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include "sunlight_screen.h"
#include "app.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define LV_SYMBOL_MOON ""
#define LV_SYMBOL_SUN  ""

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/


void watch::SunlightScreen::initialize(void)
{
  scr = lv_obj_create(nullptr, nullptr);

  lv_obj_t* arc = lv_arc_create(scr, nullptr);
  lv_arc_set_bg_angles(arc, 0, 360);
  lv_obj_set_size(arc, CONFIG_LVGL_HOR_RES_MAX - 2, CONFIG_LVGL_VER_RES_MAX - 2);
  lv_obj_align(arc, nullptr, LV_ALIGN_CENTER, 0, 0);

  lv_obj_t* indicator_arc = lv_arc_create(scr, nullptr);
  lv_arc_set_bg_angles(indicator_arc, 0, 360);
  lv_obj_set_size(indicator_arc, CONFIG_LVGL_HOR_RES_MAX - 20, CONFIG_LVGL_VER_RES_MAX - 20);
  lv_obj_align(indicator_arc, nullptr, LV_ALIGN_CENTER, 0, 0);

  lv_obj_t* sunrise_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(sunrise_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);

  lv_obj_t* sunset_label = lv_label_create(scr, nullptr);
  lv_obj_add_style(sunset_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);

  lv_obj_t* time_label = lv_label_create(scr, nullptr);
  lv_obj_set_x(time_label, 5);
  lv_obj_add_style(time_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_leco28);

#if 0
  lv_obj_t* sunlight_label = lv_label_create(scr, nullptr);
  lv_obj_set_x(sunlight_label, 5);
  lv_obj_set_y(sunlight_label, 70);
  lv_obj_add_style(sunlight_label, LV_LABEL_PART_MAIN, &g_app->display.text_style_nokia8);
#endif

  lv_obj_t* sunrise_icon = lv_label_create(scr, nullptr);
  lv_obj_add_style(sunrise_icon, LV_LABEL_PART_MAIN, &g_app->display.text_style_forkawesome14);
  lv_label_set_text(sunrise_icon, LV_SYMBOL_SUN);
  lv_obj_align(sunrise_icon, nullptr, LV_ALIGN_IN_TOP_MID, 0, 15);

  lv_obj_t* sunset_icon = lv_label_create(scr, nullptr);
  lv_obj_add_style(sunset_icon, LV_LABEL_PART_MAIN, &g_app->display.text_style_forkawesome14);
  lv_label_set_text(sunset_icon, LV_SYMBOL_MOON);
  lv_obj_align(sunset_icon, nullptr, LV_ALIGN_IN_BOTTOM_MID, 0, -30);

  lv_label_set_align(sunrise_label, LV_LABEL_ALIGN_CENTER);
  lv_label_set_align(sunset_label, LV_LABEL_ALIGN_CENTER);
#if 0
  lv_label_set_align(sunlight_label, LV_LABEL_ALIGN_CENTER);
#endif
  lv_label_set_align(time_label, LV_LABEL_ALIGN_CENTER);
}

void watch::SunlightScreen::update(void)
{  
  time_t now = time(nullptr);
  struct tm* now_tm = localtime(&now);

  /* update sun times */

  WorldClock::SunTimes st = g_app->world_clock.suntimes();

  lv_obj_t* arc = lv_obj_get_child_back(scr, nullptr);
  uint16_t night_start_angle = (uint16_t)(((float)st.sunset / (3600 * 24)) * 360);
  uint16_t night_end_angle = (uint16_t)(((float)st.sunrise / (3600 * 24)) * 360);
  lv_arc_set_rotation(arc, 90); /* 00hr on bottom */
  lv_arc_set_angles(arc, night_start_angle, night_end_angle);

  lv_obj_t* indicator_arc = lv_obj_get_child_back(scr, arc);
  uint16_t time_angle = (uint16_t)(((float)(now_tm->tm_hour * 3600 +
                                            now_tm->tm_min * 60 +
                                            now_tm->tm_sec) / (3600 * 24)) * 360);
  lv_arc_set_rotation(indicator_arc, 90); /* 00hr on bottom */
  lv_arc_set_angles(indicator_arc, time_angle - 1, time_angle + 1);

  lv_obj_t* sunrise_label = lv_obj_get_child_back(scr, indicator_arc);
  lv_obj_t* sunset_label = lv_obj_get_child_back(scr, sunrise_label);
  lv_obj_t* time_label = lv_obj_get_child_back(scr, sunset_label);

#if 0
  lv_obj_t* sunlight_label = lv_obj_get_child_back(scr, sunset_label);
#endif

  lv_label_set_text_fmt(sunrise_label, LV_SYMBOL_SUN "\n%02i:%02i:%02i", (int)(st.sunrise / 3600),
                        (int)((st.sunrise % 3600) / 60), st.sunrise % 60);
  lv_label_set_text_fmt(sunset_label, LV_SYMBOL_MOON "\n%02i:%02i:%02i", (int)(st.sunset / 3600),
                        (int)((st.sunset % 3600) / 60), st.sunset % 60);
#if 0
  lv_label_set_text_fmt(sunlight_label, "sunlight:\n%i hr. %i min.", (int)(st.sunlight / 3600),
                        (int)((st.sunlight % 3600) / 60));
#endif

  char time_str[7];
  strftime(time_str, sizeof(time_str) - 1, "%H:%M", now_tm);
  lv_label_set_text(time_label, time_str);

  lv_obj_align(sunrise_label, nullptr, LV_ALIGN_IN_TOP_MID, 0, 25);
  lv_obj_align(sunset_label, nullptr, LV_ALIGN_IN_BOTTOM_MID, 0, -18);
  lv_obj_align(time_label, nullptr, LV_ALIGN_CENTER, 0, 3);

#if 0
  lv_obj_align(sunlight_label, nullptr, LV_ALIGN_CENTER, 0, 20);
#endif
}

void watch::SunlightScreen::enter(watch::Screen* prev_screen)
{
  /* update every minute */

  g_app->set_alarm_period(60);
}
